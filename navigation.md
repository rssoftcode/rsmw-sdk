[RSMW API]()

  * [1. 欢迎](doc/api/welcome.md)
  * [2. 约定](doc/api/basic.md)
  * [3. 认证](doc/api/auth.md)
  * [4. 配置](doc/api/config.md)
  * [5. API Command](doc/api/command.md)
  * [6. API Event](doc/api/event.md)
  * [7. API Route](doc/api/route.md)
  * [附录1-SDK](doc/api/sdk.md)
  * [附录2-调试工具](doc/api/debug.md)
  * [附录3-高可用和集群](doc/api/ha.md)
  * [附录4-常见问题](doc/api/faq.md)

[RSMW SDK]()

  * [JAVA](doc/sdk/rsmw-java.md)
  * [其它语言](doc/sdk/other.md)

[gimmick:ThemeChooser](主题)