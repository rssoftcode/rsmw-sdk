# RSMW-JAVA

## 说明

“RSMW-JAVA” 是 “RSMW呼叫中心中间件” 的 JAVA版本的SDK

它提供了 RSMW 2.0 所有 API 的 Java 实现

建议采用JDK 1.8

## 如何获取？

- GIT

https://gitee.com/rssoftcode/rsmw-sdk.git

## RSMW-JAVA 使用方法

请参考SampleCode.java

```

package cc.rssoft.rsmw.sdk;

import java.util.ArrayList;
import java.util.List;

import cc.rssoft.rsmw.sdk.command.internal.SimpleResponse;
import cc.rssoft.rsmw.sdk.command.response.AgentListAllResponse;
import cc.rssoft.rsmw.sdk.command.response.AgentSipPhoneBindInfoResponse;
import cc.rssoft.rsmw.sdk.command.response.GetChannelCountResponse;
import cc.rssoft.rsmw.sdk.command.response.GetChannelResponse;
import cc.rssoft.rsmw.sdk.command.response.GetMobileInfoResponse;
import cc.rssoft.rsmw.sdk.command.response.GetSipPhoneStatusResponse;
import cc.rssoft.rsmw.sdk.command.response.OriginateResponse;

/**
 * @version 2.0
 */
public class SampleCode {

	
	/**
	 * 注意！！！！！！！！
	 * 
	 * rsmwUrl 格式请参考 RSMW 2 API 文档
	 */
	private static String rsmwUrl = "http://192.168.1.163:8080/rsmw/api/2.0/";
	
	/**
	 * 注意！！！！！！！！
	 * 
	 * apiSecret在rsmw系统的API界面中配置。
	 * 即：启用apiSecret后的apiSecret
	 */
	private static String apiSecret = "pA55w0rd";
	
	private static int connectTimeout = 2000;
	private static int readTimeout = 2000;
	
	private static RsmwServer rsmwSdk= new RsmwServer(rsmwUrl,apiSecret,connectTimeout,readTimeout);
	
	public static void main(String[] args) {
		
		//这里调用了所有的接口方法，您在测试某个接口时，请先注释掉其他的调用，以免影响您查看测试结果
		
		// 5.1 【新增Agent】
		SimpleResponse agentSaveResponse = rsmwSdk.agentSave("9999", "测试座席9", 9999L);
		System.out.println(agentSaveResponse.toJsonString());
		
		
		// 5.1 【删除Agent】
		List<String> agentAidList = new ArrayList<>();
		agentAidList.add("1055");
		agentAidList.add("1054");
		agentAidList.add("1053");
		agentAidList.add("1052");
		agentAidList.add("1051");
		agentAidList.add("1050");
		SimpleResponse agentDeleteResponse = rsmwSdk.agentDelete(agentAidList);
		System.out.println(agentDeleteResponse.toJsonString());
		
		
		// 5.1 【获取所有Agent】
		AgentListAllResponse agentListAllResponse = rsmwSdk.agentListAll();
		System.out.println(agentListAllResponse.toJsonString());
		
		
		// 5.2 【绑定】
		SimpleResponse agentSipPhoneBindResponse = rsmwSdk.bind("9999", "8001");
		System.out.println(agentSipPhoneBindResponse.toJsonString());
		
		
		// 5.2 【解绑】 根据座席解绑分机
		SimpleResponse agentSipPhoneUnBindResponse1 = rsmwSdk.unBind("9999", null);
		System.out.println(agentSipPhoneUnBindResponse1.toJsonString());
		
		
		// 5.2 【解绑】 根据分机解绑座席
		SimpleResponse agentSipPhoneUnBindResponse2 = rsmwSdk.unBind(null, "8001");
		System.out.println(agentSipPhoneUnBindResponse2.toJsonString());
		
		
		// 5.3 【绑定关系查询】 查询座席绑定的分机
		AgentSipPhoneBindInfoResponse agentSipPhoneBindInfoResponse1 = rsmwSdk.getBindInfo("9999", null);
		System.out.println(agentSipPhoneBindInfoResponse1.toJsonString());
		
		
		// 5.3 【绑定关系查询】查询分机绑定的座席
		AgentSipPhoneBindInfoResponse agentSipPhoneBindInfoResponse2 = rsmwSdk.getBindInfo(null, "8001");
		System.out.println(agentSipPhoneBindInfoResponse2.toJsonString());
		
		
		// 5.4 【置忙】 
		SimpleResponse qMemberPauseResponse = rsmwSdk.queueMemberPause("SIP/8001", "置忙原因：你猜");
		System.out.println(qMemberPauseResponse.toJsonString());
		
		
		// 5.4 【置闲】
		SimpleResponse qMemberUnPauseResponse = rsmwSdk.queueMemberUnPause("SIP/8001");
		System.out.println(qMemberUnPauseResponse.toJsonString());
		
		
		// 5.5 【实时并发数查询】
		GetChannelCountResponse getChannelCountResponse = rsmwSdk.getChannelCount();
		System.out.println(getChannelCountResponse.toJsonString());
		
		
		// 5.6 【发起呼叫】
		OriginateResponse originateResponse = rsmwSdk.originate(0,"8001","","","","8002","","","");
		System.out.println(originateResponse.toJsonString());
		
		
		// 5.7 【获取指定SipPhone的当前Channel】
		GetChannelResponse getChannelResponse = rsmwSdk.getChannel("8001");
		System.out.println(getChannelResponse.toJsonString());
		
		
		// 5.8 【转接Channel】
		SimpleResponse redircetResponse = rsmwSdk.redircet("SIP/8001-XXXXXXXX", "13391026171");
		System.out.println(redircetResponse.toJsonString());
		
		
		// 5.9 【挂断Channel】
		SimpleResponse hangupResponse = rsmwSdk.hangup("SIP/8001-XXXXXXXX");
		System.out.println(hangupResponse.toJsonString());
		
		
		// 5.10 【获取所有SipPhone状态】
		GetSipPhoneStatusResponse getSipPhoneStatusResponse = rsmwSdk.getSipPhoneStatus();
		System.out.println(getSipPhoneStatusResponse.toJsonString());
		
		
		// 5.11 【查询归属地】
		GetMobileInfoResponse getMobileInfoResponse = rsmwSdk.getMobileInfo("13391026171");
		System.out.println(getMobileInfoResponse.toJsonString());
		
	}

	
}


```
