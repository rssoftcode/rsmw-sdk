# 2. 基本约定

## 2.1 通信方式

- RSMW 采用 HTTP POST 与第三方系统进行通信，消息体采用 JSON 格式。

## 2.2 URL 及 HTTP POST JSON格式

- URL 格式

```
    http://${IP}:${PORT}/rsmw/api/${API_VERSION}/?sign=${sign}
```
例：

```
    http://192.168.100.100:18080/rsmw/api/2.0?sign=371d58f95967f0c87b95376b39ec2552
```
- HTTP 头部

注意，HTTP 请求头部的 Content-Type **必须** 如下设置

```
    Content-Type: application/json;charset=UTF-8
```

- 第三方应用向RSMW发送Command（HTTP POST Request）样例：

```json
    {
        "timestamp":1478321775260,
        "command":"${commandName}"
        "${keyX}":"${valueX}"
    }
```

- RSMW运行Command后给第三方应用的响应（HTTP POST Response）样例：

```json
    {
        "timestamp":1478321775260,
        "errCode":"${errCode}", 
        "errMsg":"${errMsg}", 
        "${keyX}":"${valueX}"
    }
```

- RSMW向第三方应用推送Event（HTTP POST Request）样例：

```json
    {
        "timestamp":1478321775260,
        "event":"${event_name}",
        "${KEY-X}":"${VALUE-X}"
    }
```
- 注1：timestamp是自1970年1月1日0时起的“毫秒数”

- 注2：调用者服务器的时间和RSMW服务器的时间误差不能超过10秒。否则系统会认为时间戳失效。

用Java获取该时间的代码为：

```java
    System.currentTimeMillis()
```

## 2.3 API的调用者

- 所有的通信均在第三方应用的***【服务器端】***与RSMW之间进行。

## 2.4 英文字母大小写

- 全部英文字母严格区分大小写

## 2.5 编码

- 全部字符均采用UTF-8编码

