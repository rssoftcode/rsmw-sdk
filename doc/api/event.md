# 6. API Event

## 6.1 channel的接通和断开

触发时机：两个channel接通或断开时

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| bridge | 接通或挂断（true，false） | Boolean | 64 | |
| areaCode | 区号 | String | 64 | |
| province | 省 | String | 64 | |
| city | 市 | String | 64 | |
| operator | 运营商 | String | 64 | || 


```json
    {
        "timestamp":1478321775260,
        "event":"bridgeEvent",
        "bridge":true,
        "channel1":"SIP/8001-XXXXXXXX",
        "uniqueId1":"1486973220.28",
        "callerIdNumber1":"8001",
        "channel2":"SIP/gw1-XXXXXXXX",
        "uniqueId2":"1486973221.00",
        "callerIdNumber2":"13391026171"
    }
```

## 6.2 channel的状态改变【弹屏】

触发时机：channel的状态改变时

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| channel | 通道名称 | String | 64 |  |
| uniqueId | 通道id | String | 64 | |
| channelState | 通道状态【RING,RINGING,UP,DOWN】 | String | 64 | |
| callerIdName | 主叫号callerIdName | String | 64 | |
| callerIdNumber | 主叫名callerIdNumber | String | 64 | |
| connectedLineName | 对端主叫号 | String | 64 | |
| connectedLineNumber | 对端主叫名 | String | 64 | ||

```json
    {
        "timestamp":1478321775260,
        "event":"newStateEvent",
        "channel":"SIP/8001-XXXXXXXX",
        "uniqueId":"1486973220.28",
        "channelState":"RINGING",
        "callerIdName":"8001",
        "callerIdNumber":"8001",
        "connectedLineName":"13391026171",
        "connectedLineNumber":"13391026171"
    }
```

## 6.3 channel路由

触发时机：channel被路由到新的exten时

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| accessDate | 日期 | String | 64 | |
| uniqueId | channel唯一标识 | String | 64 | |
| channel | channel名称 | String | 64 | |
| callerIdNumber | 主叫号 | String | 64 | |
| srcGateway | 本channel的来源网关 | String | 64 | |
| srcAccessNumber | 本channel的来源接入号 | String | 64 | |
| dstGateway | 本channel的目标网关 | String | 64 | |
| dstAccessNumber | 本channel的目标接入号 | String | 64 | |
| exten | 本channel当前路由到的分机号 | String | 64 | |
| extenType | 本channel当前路由到的分机号类型 | String | 64 | |
| extenDescription | 本channel当前路由到的分机号描述 | String | 64 | | |
| userData | 用户自定义数据 | String | 128 | 是 |


```json
    {
        "timestamp":1478321775260,
        "event":"channelRouteEvent",
        "accessDate":"2017-11-22 01:23:45",
        "uniqueId":"1486973220.28",
        "channel":"SIP/gw1-XXXXXXXX",
        "callerIdNumber":"13391026171",
        "srcGateway":"gw1",
        "srcAccessNumber":"60000000",
        "dstGateway":"gw2",
        "dstAccessNumber":"61111111",
        "exten":"7001",
        "extenType":"QUEUE",
        "extenDescription":"客服组"
    }
```

## 6.4 CDR【话单】

触发时机：通话结束时

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| startTime | 通话开始时间 | String | 64 | 是 |
| answerTime | 通话应答时间 | String | 64 | |
| endTime | 通话结束时间 | String | 64 | 是 |
| answered | 是否应答 | Boolean |  | 是 |
| duration | 持续时长 | Integer | 64 | 是 |
| billSeconds | 接通时长 | Integer | 64 | 是 |
| callDirection | 方向【INNER（内部呼叫，分机打分机）, OUTER（外转外，手机打手机）, INBOUND（呼入，手机打分机）, OUTBOUND（呼出，分机打手机）, ORIGINATE（API发起的呼叫）】 | String | 64 | 是 |
| callerId | 主叫字符串（包括callerIdNumber和callerIdName） | String | 64 | 是 |
| src | 主叫 | String | 64 | 是 |
| srcGateway | 主叫网关 | String | 64 | |
| srcAccessNumber | 主叫接入号 | String | 64 | |
| srcAgentAid | 主叫agentAid | String | 64 | |
| srcAgentname | 主叫agent姓名 | String | 64 | |
| srcChannel | 主叫channel | String | 64 | 是 |
| dst | 被叫 | String | 64 | 是 |
| dstGateway | 被叫网关 | String | 64 | |
| dstAccessNumber | 被叫接入号 | String | 64 | |
| dstAgentAid | 被叫agentAid | String | 64 | |
| dstAgentname | 被叫agent姓名 | String | 64 | |
| dstChannel | 被叫channel | String | 64 | 是 |
| serverId | 产生本话单的服务器id | String | 64 | 是 |
| recordUrl | 录音相对路径 | String | 64 | 是 |
| uniqueId | 本通话的唯一标识 | String | 64 | 是 |
| callUuid | 本通话的唯一标识（api指定） | String | 64 | 是 |
| disposition | 通话结果【NO ANSWER，FAILED，BUSY，ANSWERED，UNKNOWN】 | String | 64 | 是 |
| userData | 用户自定义数据 | String | 128 | 是 |

```json
{
    "timestamp":1478321775260,
    "event":"cdrEvent",
    "startTime":"2017-02-13 15:58:59",
    "answerTime":"",
    "endTime":"2017-02-13 15:59:07",
    "answered":false,
    "duration":8,
    "billSeconds":0,
    "callDirection":"INNER",
    "callerId":"8002 <8002>",
    "src":"8002",
    "srcGateway":"",
    "srcAccessNumber":"",
    "srcAgentAid":"",
    "srcAgentname":"",
    "srcChannel":"SIP/8002-00000010",
    "dst":"8001",
    "dstGateway":"",
    "dstAccessNumber":"",
    "dstAgentAid":"",
    "dstAgentName":"",
    "dstChannel":"SIP/8001-00000011",
    "dstContext":"SIPPHONE",
    "serverId":"asterisk1",
    "recordUrl":"/2017/02/13/1486972739.26.wav",
    "uniqueId":"1486973220.28",
    "callUuid":"b37663e3695c41af978ed35a616fdd49",
    "disposition":"ANSWERED"
}
```

## 6.5 服务质量评价

触发时机：发生服务质量评价时

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| sqeDate | 日期 | String | 64 | |
| queueId | 技能组id | Long | 64 | |
| queueName | 技能组名称 | String | 64 | |
| customerPhoneNumber | 打分者的电话号码 | String | 64 | |
| agentAid | 被打分的agentAid | String | 64 | |
| agentName | 被打分的agentName | String | 64 | |
| uniqueId | channel唯一标识 | String | 64 | |
| score | 得分 | Integer | 64 | | |

```json
    {
        "timestamp":1478321775260,
        "event":"sqeEvent",
        "sqeDate":"2017-11-22 01:23:45",
        "queueId":1,
        "queueName":"客服1组",
        "customerPhoneNumber":"13391026171",
        "agentAid":"1001",
        "agentName":"张三",        
        "uniqueId":"1486973220.28",
        "score":1
    }
```

## 6.6 技能组成员置忙置闲通知事件

触发时机：技能组成员置忙置闲时

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| queueMemberInterface | 技能组成员所使用的设备拨号字符串，格式为：SIP/${sipPhoneName} | String | 64 | 是 |
| reason | 置忙原因 | String | 64 | |  |

- 【置忙】
Request:
```json
    {
        "timestamp":1478321775260,
        "command":"queueMemberPause",
        "queueMemberInterface":"SIP/8001",
        "reason":"开会"
    }
```

- 【置闲】
Request:
```json
    {
        "timestamp":1478321775260,
        "command":"queueMemberUnPause",
        "queueMemberInterface":"SIP/8001"
    }
```

## 6.7 座席绑定解绑分机通知事件

触发时机：在用户登录时调用【绑定】；在用户注销时调用【解绑】

- 用途：将Agent和SipPhone建立“一对一对的应关系”告诉rsmw“哪个Agent在使用哪个SipPhone”。
- 调用时机：在用户登录时调用【绑定】；在用户注销时调用【解绑】

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| agentAid | 第三方应用中用户的唯一标识，一般是id或用户名 | String | 64 | 是 |
| sipPhoneName | 所要绑定的SIPPHONE的分机号 | String | 64 | |  |

- 【绑定】
Request:
```json
    {
        "timestamp":1478321775260,
        "command":"bind",
        "agentAid":"1001",
        "sipPhoneName":"8001"
    }
```

- 【解绑】
Request:
```json
    {
        "timestamp":"1478321782349",
        "command":"unBind",
        "agentAid":"1001"
    }
```
```json
    {
        "timestamp":"1478321782349",
        "command":"unBind",
        "sipPhoneName":"8001"
    }
```