接口返回码对照表

| 返回码 | 说明 |
| ---- | :--: |
| 0 | 调用成功 |
| -1 | 未授权 |
| -10000 | 非法签名 |
| -10001 | 非法的JSON格式 |
| -10002 | 缺少command字段 |
| -10003 | 未知的命令,请检查URL中的命令名 |
| -10004 | 缺少timestamp字段 |
| -10005 | 您的时间戳与RSMW系统的时间误差超过了60秒。请检查系统时间是否准确。您可以通过配置ntp服务校正系统时间 |
| -10006 | 缺少必填字段 |
| -20000 | 参数值有误 |
| -30000 | 呼叫速度过快 |
| -30001 | 未查询到相关数据 |
| -30002 | 没有找到SIP分机 |
| -30003 | 没有对端channel |
| -99999 | 未知错误 |

# 5. API Command

## 5.1 分机的操作接口

- 用途：获取和设置分机的状态

- 接口：增/删/改/查/置忙/置闲

接口参数

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| name | 分机号 | String | 64 | 是 |
| secret | 认证密码，用于SIP Auth | String | 64 | 是 |
| timeout | 分机超时 | Integer |  | 是 |
| callerIdNumber | 分机的指定主叫号 | String | 64 | |
| dstGateway | 分机的指定网关 | String | 64 | |
| canNotCallWithoutBind | 分机未绑定座席信息时不允许呼叫 | Boolean |  | 是 |
| canLdd | 是否允许本地呼叫 | Boolean |  | 是 |
| canDdd | 是否允许国内长途 | Boolean |  | 是 |
| canIdd | 是否允许国际长途 | Boolean |  | 是 |
| hideCallerId | 是否隐藏来电显示 | Boolean |  | 是 |
| codec1 | 语音编码，固定值[none,all,alaw,ulaw,g729,g723,g726,g722,gsm,ilbc] | String | 64 | 是 |
| codec2 | 语音编码，固定值[none,all,alaw,ulaw,g729,g723,g726,g722,gsm,ilbc] | String | 64 | 是 |
| codec3 | 语音编码，固定值[none,all,alaw,ulaw,g729,g723,g726,g722,gsm,ilbc] | String | 64 | 是 |
| codec4 | 语音编码，固定值[none,all,alaw,ulaw,g729,g723,g726,g722,gsm,ilbc] | String | 64 | 是 |
| callForwardType | 呼叫转移类型，固定值[none,forwardAll,forwardOnBusy,forwardOnNoanswer] | String | 64 | 是 |
| callForwardNumber | 呼叫转移到这个号码 | String | 64 | |
| callForwardGateway | 呼叫转移所使用到网关 | String | 64 | |
| callForwardAccessNumber | 呼叫转移所使用到接入号 | String | 64 | ||

- 【新增分机】

Request 请求:
```json
{
    "timestamp":1478321775260,
    "command":"sipPhoneSave",
    "sipPhone":{
        "name":"8001",
        "secret":"pA55w0rd",
        "timeout":60,
        "callerIdNumber":"66668888",
        "dstGateway":"gw0",
        "canNotCallWithoutBind":true,
        "canLdd":true,
        "canDdd":true,
        "canIdd":false,
        "hideCallerId":false,
        "codec1":"PCMA",
        "codec2":"PCMU",
        "codec3":"g729",
        "codec4":"none",
        "callForwardType":"forwardOnBusy",
        "callForwardNumber":"13391026171",
        "callForwardGateway":"gw0",
        "callForwardAccessNumber":"66668888"
    }
}
```

Response 响应:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS",
    "id":1
}
```

- 【删除分机】

Request 请求:
```json
{
    "timestamp":"1478321782349",
    "command":"sipPhoneDelete",
    "idList":[1,2,3]
}
```

Response 响应:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS"
}
```

- 【修改分机】

Request 请求:
```json
{
    "timestamp":1478321775260,
    "command":"sipPhoneUpdate",
    "sipPhone":{
        "id":1,
        "name":"8001",
        "secret":"pA55w0rd",
        "timeout":60,
        "callerIdNumber":"gw0",
        "dstGateway":"66668888",
        "canNotCallWithoutBind":true,
        "canLdd":true,
        "canDdd":true,
        "canIdd":false,
        "hideCallerId":false,
        "codec1":"PCMA",
        "codec2":"PCMU",
        "codec3":"g729",
        "codec4":"none",
        "callForwardType":"forwardOnBusy",
        "callForwardNumber":"13391026171",
        "callForwardGateway":"gw0",
        "callForwardAccessNumber":"66668888"
    }
}
```

Response 响应:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS"
}
```

- 【查询所有分机】

Request 请求:
```json
{
    "timestamp":"1478321782349",
    "command":"sipPhoneGetAll"
}
```

Response 响应:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS",
    "result": [
        {
            "id":1,
            "name":"8001",
            "secret":"pA55w0rd",
            "timeout":60,
            "callerIdNumber":"gw0",
            "dstGateway":"66668888",
            "canNotCallWithoutBind":true,
            "canLdd":true,
            "canDdd":true,
            "canIdd":false,
            "hideCallerId":false,
            "codec1":"PCMA",
            "codec2":"PCMU",
            "codec3":"g729",
            "codec4":"none",
            "callForwardType":"forwardOnBusy",
            "callForwardNumber":"13391026171",
            "callForwardGateway":"gw0",
            "callForwardAccessNumber":"66668888"
        },
        {
            "id":2,
            "name":"8002",
            "secret":"pA55w0rd",
            "timeout":60,
            "callerIdNumber":"gw0",
            "dstGateway":"66668888",
            "canNotCallWithoutBind":true,
            "canLdd":true,
            "canDdd":true,
            "canIdd":false,
            "hideCallerId":false,
            "codec1":"PCMA",
            "codec2":"PCMU",
            "codec3":"g729",
            "codec4":"none",
            "callForwardType":"forwardOnBusy",
            "callForwardNumber":"13391026171",
            "callForwardGateway":"gw0",
            "callForwardAccessNumber":"66668888"
        }       
    ]
}
```

- 分机置忙/置闲

接口参数

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| queueMemberInterface | 技能组成员所使用的设备拨号字符串，格式为：SIP/${sipPhoneName} | String | 64 | 是 |
| reason | 置忙原因 | String | 64 | |  |

- 【置忙】
Request:
```json
{
    "timestamp":1478321775260,
    "command":"queueMemberPause",
    "queueMemberInterface":"SIP/8001",
    "reason":"开会"
}
```
- 【置闲】
Request:
```json
{
    "timestamp":1478321775260,
    "command":"queueMemberUnPause",
    "queueMemberInterface":"SIP/8001"
}
```

- Response
```json
{
    "errCode":"0",
    "errMsg":"SUCCESS",
    "timestamp":1520567493222
}
```

## 5.2 发起呼叫

- 用途：控制中间件发起呼叫

- 效果：中间件第一路呼叫src,利用网关名称srcGateway和接入号srcAccessNumber进行呼叫，待src呼通并接听后,呼叫第二路dst,利用网关名称dstGateway和接入号dstAccessNumber进行呼叫,待dst呼通并接听后,将src产生的Channel和dst产生的Channel进行接通,两方即可开始通话。

- 注意：1.呼叫号码src或dst不是内部号码，需要网关和接入号，是内部号码可不需要网关和接入号，建议不管呼叫内部号码还是外部号码都设置网关和接入号。
       2.src和dst两个号码呼通后系统会自动将两个Channel进行接通，调用者无需操作。

接口参数

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| absoluteTimeout | 通话超时时间（秒）。超时后自动挂断 | Integer | 32 | |
| src | 第一路呼叫的号码 | String | 64 | 是 |
| srcGateway | 第一路呼叫所需要走的gateway。若src不是内部分机，则此字段必须填写 | String | 64 | |
| srcAccessNumber | 第一路呼叫所需要用的主叫号。若src不是内部分机，则此字段必须填写 | String | 64 | |
| srcAnnounceMediaUrl | 第一路呼叫接通后所听到的提示音（仅支持8000赫兹、16位、单声道的wav音频文件） | String | 64 | |
| dst | 第二路呼叫的号码 | String | 64 | 是 |
| dstGateway | 第二路呼叫所需要走的gateway。若dst不是内部分机，则此字段必须填写 | String | 64 | |
| dstAccessNumber | 第二路呼叫所需要用的主叫号。若dst不是内部分机，则此字段必须填写 | String | 64 | |
| dstAnnounceMediaUrl | 第二路呼叫接通后所听到的提示音（仅支持8000赫兹、16位、单声道的wav音频文件） | String | 64 | |
| userData | 用户自定义数据。此数据会在之后的event中原封不动地带回来 | String | 128 | |
| callUuid | 呼叫的唯一标识 | Integer | 64 | | 
| queueLength | RSMW 内部呼叫队列中，当前排队的呼叫请求数量 | Integer | 64 | |  |

Request 请求:
```json
{
    "timestamp":1478321775260,
    "command":"originate",
    "absoluteTimeout":300,
    "src":"8001",
    "srcGateway":"myGatewayName1",
    "srcAccessNumber":"60172133",
    "srcAnnounceMediaUrl":"http://ip:port/path",
    "dst":"13370021000",
    "dstGateway":"myGatewayName2",
    "dstAccessNumber":"60172133",
    "dstAnnounceMediaUrl":"http://ip:port/path",
    "userData":"用于自定义数据"
}
```

Response 响应:
```json
{
    "timestamp":1478321775260,
    "errCode":"0",
    "errMsg":"SUCCESS",
    "callUuid":"b37663e3695c41af978ed35a616fdd49",
    "queueLength":10
}
```

```json
{
    "timestamp":1478321775260,
    "errCode":"-10004",
    "errMsg":"网关并发达到上限"
}
```

```json
{
    "timestamp":1478321775260,
    "errCode":"-20001",
    "errMsg":"呼叫队列已满"
}
```

## 5.3 获取分机通话Channel和对Channel的操作

- 注意：只有通话中的分机才有Channel，Channel是一通通话的唯一标识，用来操作当前通话。

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| sipPhoneName | sipPhone的分机号 | String | 64 | 是 |
| channel | sipPhone的channel | String | 64 | |
| connectedChannel | 正与sipPhone接通的对方channel | String | 64 | |  |

Request:
```json
{
    "timestamp":1478321775260,
    "command":"getChannel",
    "sipPhoneName":"8001"
}
```

Response:
```json
{
    "timestamp":1478321775260,
    "errCode":"0",
    "errMsg":"SUCCESS",
    "channel":"SIP/8001-XXXXXXXX",
    "connectedChannel":"SIP/gw1-XXXXXXXX"
}
```

- 对获取分机通话Channel的操作

- 主要操作：桥接Channel/转接Channel/挂断Channel

- 桥接Channel

- 用途：将两个channel接通，让它们之间可以通话

注意：由于channel1和channel2的顺序会影响cdr中的uniqueid，进而影响录音和cdr的对应关系，所以需要把“主叫”channel赋给channel1

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| channel1 | 希望桥接的channel1 | String | 64 | 是 |
| channel2 | 希望桥接的channel2 | String | 64 | 是 |

Request:
```json
{
    "timestamp":1478321775260,
    "command":"bridge",
    "channel1":"SIP/8001-XXXXXXXX",
    "channel2":"SIP/gw1-XXXXXXXX"
}
```

- 转接Channel

- 用途：将指定的通话转接到指定的exten

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| channel | 希望转接的channel名称 | String | 64 | 是 |
| context | 希望将channel转接到的目标context,一般情况下填写'core_route' | String | 64 | 是 |
| exten | 希望将channel转接到的目标分机号 | String | 64 | 是 |
| dstGateway | 呼叫目标exten所用的gateway | String | 64 |  |
| dstAccessNumber | 呼叫目标exten所用的accessNumber | String | 64 |  |
| extraChannel | 希望转接的channel名称 | String | 64 |  |
| extraContext | 希望将extraChannel转接到的目标context | String | 64 | 是 |
| extraExten | 希望将channel转接到的目标分机号 | String | 64 |  |
| extraDstGateway | 呼叫目标exten所用的gateway | String | 64 |  |
| extraDstAccessNumber | 呼叫目标exten所用的accessNumber | String | 64 |  |  |

Request:
```json
{
    "timestamp":1478321775260,
    "command":"redirect",
    "channel":"SIP/8001-XXXXXXXX",
    "context":"core_route",
    "exten":"13391026171",
    "dstGateway":"gw1",
    "dstAccessNumber":"66668888",
    "extraChannel":"SIP/gw1-XXXXXXXX",
    "extraExten":"8002",
    "extraDstGateway":"gw1",
    "extraDstAccessNumber":"66669999"
}
```

- 挂断Channel

- 挂断

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| channel | 希望挂断的channel名称 | String | 64 | 是 |

Request:
```json
{
    "timestamp":1478321775260,
    "command":"hangup",
    "channel":"SIP/8001-XXXXXXXX"
}
```

## 5.4 对接座席的操作接口

- 用途：将第三方系统的登录用户，通过接口同步到系统的座席。

- 接口：新增/删除/获取所有座席/同步座席

- 注意：不支持修改座席，如需修改座席请删除后新建，并重新绑定相应的分机。

接口参数

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| aid | 第三方应用中用户的唯一标识 | String | 64 | 是 |
| pwd | 座席密码 | String | 64 | 是 |
| agentName | 姓名 | String | 64 | 是|
| jobNumber | 工号 | Long | 20 |是 | 


- 【新增座席】

Request 请求:
```json
{
    "timestamp":1478321775260,
    "command":"agentSave",
    "agent":{
        "aid":"21232F297A57A5A743894A0E4A801FC3",
        "pwd":"password123",
        "agentName":"张三",
        "jobNumber":9527
    }
}
```

Response 响应:
```json
{
    "errCode":"0",
    "errMsg":"SUCCESS",
    "id":1,
    "timestamp":1520567707630
}
```

- 【删除座席】

Request 请求:
```json
{
    "timestamp":"1478321782349",
    "command":"agentDelete",
    "agentAidList":["21232F297A57A5A743894A0E4A801FC3","0192023A7BBD73250516F069DF18B500"]
}
```

Response 响应:
```json
{
    "errCode":"0",
    "errMsg":"SUCCESS",
    "timestamp":1520567793875
}
```

- 【获取所有座席】

Request 请求:
```json
{
    "timestamp":"1478321782349",
    "command":"agentGetAll"
}
```

Response 响应:
```json
{
    "errCode":"0",
    "errMsg":"SUCCESS",
    "result":{
        "0192023A7BBD73250516F069DF18B500":{
            "agentName":"李四",
            "aid":"0192023A7BBD73250516F069DF18B500",
            "extColumns":{
            },
            "id":19,
            "jobNumber":9528,
            "pwd":"password456"
        },
        "21232F297A57A5A743894A0E4A801FC3":{
            "agentName":"张三",
            "aid":"21232F297A57A5A743894A0E4A801FC3",
            "extColumns":{
            },
            "id":18,
            "jobNumber":9527,
            "pwd":"password123"
        }
    },
    "timestamp":1520565496789
}
```

- 【同步座席】

注意：同步会删除座席之前所有的数据，以同步数据为准。

Request 请求:
```json
{
    "timestamp":"1478321782349",
    "command":"agentSync",
    "agentList":[
        {
            "agentName": "张三",
            "aid": "21232F297A57A5A743894A0E4A801FC3",
            "pwd":"password123",
            "jobNumber": 9527
        },
        {
            "agentName": "李四",
            "aid": "0192023A7BBD73250516F069DF18B500",
            "pwd":"password456",
            "jobNumber": 9528
        }
    ]
}
```

Response 响应:
```json
{
    "errCode":"0",
    "errMsg":"SUCCESS",
    "timestamp":1520567879557
}
```

## 5.5 座席与分机的操作接口

- 用途：设置第三方系统的用户使用指定的分机

- 接口：绑定/解绑/绑定关系查询

接口参数

| 字段 | 说明 | 类型 | 长度 | 必填 

| ---- | ---- | :--: | :--: | :--: |

| agentAid | 第三方应用中用户的唯一标识 | String | 64 | 是 |  
| sipPhoneName | 所要绑定的SIP分机号 | String | 64 |是 |  

- 【绑定】
Request 请求:
```json
{
    "timestamp":1478321775260,
    "command":"bind",
    "agentAid":"21232F297A57A5A743894A0E4A801FC3",
    "sipPhoneName":"8001"
}
```

Response 响应:
```json
{
    "errCode":"0",
    "errMsg":"SUCCESS",
    "timestamp":1520567962066
}
```

- 【解绑方式一：根据座席Aid解绑】
Request 请求:
```json
{
    "timestamp":"1478321782349",
    "command":"unBind",
    "agentAid":"21232F297A57A5A743894A0E4A801FC3"
}
```
- 【解绑方式二：根据分机号解绑】
```json
{
    "timestamp":"1478321782349",
    "command":"unBind",
    "sipPhoneName":"8001"
}
```

Response 响应:
```json
{
    "errCode":"0",
    "errMsg":"SUCCESS",
    "timestamp":1520568033028
}
```

- 【绑定关系查询方式一：根据座席Aid查询】
- Request 请求:
```json
{
	"timestamp":1478321775260,
    "command":"getBindInfo",
    "agentAid":"21232F297A57A5A743894A0E4A801FC3"
}
```
- 【绑定关系查询方式二：根据分机号查询】
```json
{
	"timestamp":1478321775260,
    "command":"getBindInfo",
    "sipPhoneName":"8001"
}
```
- Response
```json
{
    "timestamp":1478321775260,
    "errCode":"0",
    "errMsg":"SUCCESS",
    "agentAid":"1001",
    "sipPhoneName":"8001"
}
```

## 5.6 实时并发数查询

- 用途：查询系统当前的并发通话Channel的总数
- 调用时机：一般在发起呼叫前调用，用来判断系统的负载，决定是否要发起呼叫

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| channels | 实时通道数 | Integer | 64 | |
| calls | 实时通话数 | Integer | 64 | |  |

说明：一次呼叫会产生主叫Channel和被叫Channel两个Channel，两个Channel接通会产生一个通话，如果主叫或被叫未呼通将不会产生Channel,因此不产生通话，其中主叫未呼通也将不会呼叫被叫，呼叫被叫号码是在主叫号码接通以后才开始呼叫被叫号码。

Request 请求:
```json
{
    "timestamp":1478321775260,
    "command":"getChannelCount"
}
```

Response 响应:
```json
{
    "timestamp":1478321775260,
    "errCode":"0",
    "errMsg":"SUCCESS",
    "channels":201,
    "calls":100,
}
```

## 5.7 呼叫保持

- 用途：hold、unhold指定的sipPhone

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| sipPhoneName | sipPhone的分机号 | String | 64 | 是 |

- 【呼叫保持】
Request 请求:
```json
{
    "timestamp":1478321775260,
    "command":"hold",
    "sipPhoneName":"8001"
}
```

- 【取消呼叫保持】
Request 请求:
```json
{
    "timestamp":1478321775260,
    "command":"unhold",
    "sipPhoneName":"8001"
}
```

Response 响应:
```json
{
    "timestamp":"1478321782349",
    "errCode":"0",
    "errMsg":"SUCCESS"
}
```
## 5.8 获取所有SipPhone状态

注意：这里没有获取指定的状态。
提示：如需调用请开线程获取所有的状态保存到本地服务器，页面直接访问本地服务器的获取的状态。

用途：查询所有的sipPhone状态

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| online | sipPhone是否注册在线 | Boolean |  | |
| regStatus | sipPhone当前注册状态描述 | String | 64 | |
| currentChannel | sipPhone当前channel名称 | String | 64 | |
| channelState | sipPhone当前通话状态【DOWN（挂机）,RINGING（振铃（被动））,RING（振铃（主动））,UP(已接通)】 | String | 64 | |
| connectedChannel | 正与sipPhone接通的对端channel名称 | String | 64 | |
| agent | 当前绑定该sipPhone的坐席 | Object |  | |
| agent.id | agent的id | Long | 64 | |
| agent.aid | agent的aid | String | 64 | |
| agent.agentName | agent的姓名 | String | 64 | |
| agent.jobNumber | agent的工号 | Long | 64 | |
| sipPhone | sipPhone对象 | Object |  | |
| sipPhone.id | sipPhone的id | Long | 64 | |
| sipPhone.name | sipPhone的分机号 | String | 64 | |
| sipPhone.secret | sipPhone注册密码 | String | 64 | |
| sipPhone.callerIdNumber | sipPhone主叫号 | String | 64 | |
| sipPhone.canLdd | 允许呼叫本地号码 | Boolean |  | |
| sipPhone.canDdd | 允许呼叫国内长途 | Boolean |  | |
| sipPhone.canIdd | 允许呼叫国际长途 | Boolean |  | |  |


Request:
```json
{
    "timestamp":1478321775260,
    "command":"getSipPhoneStatus"
}
```

Response:
```json
{
    "timestamp":1478321775260,
    "errMsg": "SUCCESS",
    "errCode": "0",
    "result": {
        "8001": {
            "online": true,
            "currentChannel": "",
            "channelState": "DOWN",
            "regStatus": "OK (29 ms)",
            "connectedChannel": "",
            "agent": {
                "id": 1,
                "aid": "1001",
                "agentName": "1001",
                "jobNumber": 1001,
                "extColumns": {}
            },
            "sipPhone": {
                "id": 1,
                "name": "8001",
                "secret": "aaa111",
                "callerIdNumber": "",
                "canLdd": true,
                "canDdd": true,
                "canIdd": false,
                "extColumns": {}
            }
        },
        "8002":{
            ...
        }
    }
}
```

## 5.9 获取所有Gateway状态

注意：这里没有获取指定的状态。
提示：如需调用请开线程获取所有的状态保存到本地服务器，页面直接访问本地服务器的获取的状态。

Request:
```json
{
    "timestamp":1478321775260,
    "command":"getGatewayStatus"
}
```

Response:
```json
{
  "errCode": "0",
  "errMsg": "SUCCESS",
  "result": {
    "gw1": {
      "timestamp": 1494574167308,
      "gatewayName": "gw1",
      "registyStatus": "Registered",
      "gatewayPrefix": "",
      "online": true,
      "status": "OK (1 ms)",
      "historyChannelCount": [0,0,0,0,0,0,0,0,0,0,0,0],
      "channels": {        
        "SIP/gw1-0000013b": {
          "callerIdNumber": "13391026171",
          "connectedLineNumber": "60650895",
          "connectedLineName": "60650895",
          "channel": "SIP/gw1-0000013b",
          "channelStateDesc": "Down",
          "uniqueId": "1494574804.360",
          "channelState": 0,
          "createDate": 1494574808899
        }
      },
      "gateway": {
        "codec1": "alaw",
        "extension": "",
        "codec4": "ilbc",
        "regPort": "5060",
        "codec2": "ulaw",
        "dtmf": "rfc2833",
        "prefix": "",
        "codec3": "g729",
        "secret": "",
        "mode": "REG_OUT",
        "regHost": "139.196.207.207",
        "regDomain": "139.196.207.207",
        "port": "5060",
        "regSecret": "13391026171",
        "regUsername": "60650894",
        "host": "139.196.207.207",
        "name": "gw1",
        "id": 1,
        "regAuthusername": "60650894"
      }
    }
  }
}
```

## 5.10 获取所有技能组状态

注意：这里没有获取指定的状态。
提示：如需调用请开线程获取所有的状态保存到本地服务器，页面直接访问本地服务器的获取的状态。

用途：查询所有的技能组状态

Request:
```json
{
    "timestamp":1478321775260,
    "command":"getQueueStatus"
}
```

Response:
```json
{
  "timestamp": 1498020089618
  "errCode": "0",
  "errMsg": "SUCCESS",
  "result": {
    "6001": {
      "needClean": false,
      "queue": {
        "agentTimeout": 10,
        "announceFrequency": 90,
        "announceJobNumber": "none",
        "autoCleanMember": true,
        "autoCleanMemberTime": "0:00:00",
        "autoPause": "no",
        "destAfterTimeout": "",
        "destTypeAfterTimeout": "_hangup",
        "dstAccessNumber": "",
        "dstGateway": "",
        "enableAnnounce": false,
        "exten": "6001",
        "language": "",
        "maxLength": 0,
        "minAnnounceFrequency": 15,
        "musicClassMohId": 1,
        "name": "6001",
        "queueTimeout": 1800,
        "serviceLevel": 15,
        "sqeEnable": false,
        "strategy": "rrordered",
        "wrapuptime": 5
      },
      "queueEntryList": [],
      "queueMemberEventMap": {
        "SIP/8002": {
          "callsTaken": 0,
          "internalActionId": "735669884_105771",
          "isincall": 0,
          "lastCall": 0,
          "location": "SIP/8002",
          "membership": "dynamic",
          "name": "SIP/8002",
          "paused": false,
          "penalty": 5,
          "queue": "6001",
          "stateinterface": "SIP/8002",
          "status": 1
        }
      },
      "queueParams": {
        "abandoned": 0,
        "calls": 0,
        "completed": 0,
        "holdTime": 0,
        "max": 0,
        "queue": "6001",
        "serviceLevel": 15,
        "serviceLevelPerf": 0,
        "strategy": "rrordered",
        "talkTime": 0,
        "weight": 0
      },
      "queueSummary": {
        "available": 1,
        "callers": 0,
        "holdTime": 0,
        "loggedIn": 1,
        "longestHoldTime": 0,
        "queue": "6001",
        "talkTime": 0
      }
    }
  }
}
```

## 5.11 查询归属地

用途：查询手机号归属地信息

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| mobilePhoneNumber | 手机号 | String | 64 | 是 |
| areaCode | 区号 | String | 64 | |
| province | 省 | String | 64 | |
| city | 市 | String | 64 | |
| operator | 运营商 | String | 64 | |  |

Request:
```json
{
    "timestamp":1478321775260,
    "command":"getMobileInfo",
    "mobilePhoneNumber":"13391026171"
}
```

Response:
```json
{
    "timestamp":1478321775260,
    "errMsg": "SUCCESS",
    "errCode": "0",
    "areaCode":"021",
    "province":"上海",
    "city":"上海",
    "operator":"电信"
}
```  

## 5.12 查询CDR

用途：根据uniqueid或者callUuid查询cdr

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| startTime | 通话开始时间 | String | 64 | 是 |
| answerTime | 通话应答时间 | String | 64 | |
| endTime | 通话结束时间 | String | 64 | 是 |
| answered | 是否应答 | Boolean |  | 是 |
| duration | 持续时长 | Integer | 64 | 是 |
| billSeconds | 接通时长 | Integer | 64 | 是 |
| callDirection | 方向【INNER（内部呼叫，分机打分机）, OUTER（外转外，手机打手机）, INBOUND（呼入，手机打分机）, OUTBOUND（呼出，分机打手机）, ORIGINATE（API发起的呼叫）】 | String | 64 | 是 |
| callerId | 主叫字符串（包括callerIdNumber和callerIdName） | String | 64 | 是 |
| src | 主叫 | String | 64 | 是 |
| srcGateway | 主叫网关 | String | 64 | |
| srcAccessNumber | 主叫接入号 | String | 64 | |
| srcAgentAid | 主叫agentAid | String | 64 | |
| srcAgentname | 主叫agent姓名 | String | 64 | |
| srcChannel | 主叫channel | String | 64 | 是 |
| dst | 被叫 | String | 64 | 是 |
| dstGateway | 被叫网关 | String | 64 | |
| dstAccessNumber | 被叫接入号 | String | 64 | |
| dstAgentAid | 被叫agentAid | String | 64 | |
| dstAgentname | 被叫agent姓名 | String | 64 | |
| dstChannel | 被叫channel | String | 64 | 是 |
| serverId | 产生本话单的服务器id | String | 64 | 是 |
| recordUrl | 录音相对路径 | String | 64 | 是 |
| uniqueId | 本通话的唯一标识 | String | 64 | 是 |
| callUuid | 本通话的唯一标识（api指定） | String | 64 | 是 |
| disposition | 通话结果【NO ANSWER，FAILED，BUSY，ANSWERED，UNKNOWN】 | String | 64 | 是 |


Request:
```json
{
    "timestamp":1478321775260,
    "command":"getCdrByUniqueid",
    "uniqueid":"1486973220.28"
}
```

```json
{
    "timestamp":1478321775260,
    "command":"getCdrByCallUuid",
    "callUuid":"b37663e3695c41af978ed35a616fdd49"
}
```

Response:
```json
{
	"timestamp":1478321775260,
    "errCode": "0",
    "errMsg": "SUCCESS",
    "cdr": {
	    "answerTime": 1488785811000,
	    "answered": true,
	    "billSeconds": 1,
	    "callDirection": "ORIGINATE",
	    "callUuid": "cad725eb80cb45e3b36b77ea6218d3ad",
	    "callerId": "8001",
	    "disposition": "ANSWERED",
	    "dst": "8002",
	    "dstAccessNumber": "",
	    "dstAgentAid": "",
	    "dstAgentName": "",
	    "dstChannel": "SIP/8002-00000045",
	    "dstContext": "SIPPHONE",
	    "dstGateway": "",
	    "duration": 6,
	    "endTime": 1488785812000,
	    "id": 3,
	    "lastApplication": "Dial",
	    "lastData": "SIP/8002,60,t",
	    "recordUrl": "/2017/03/06/1488785806.68.wav",
	    "scoreComment": "",
	    "serverId": "server1",
	    "src": "8001",
	    "srcAccessNumber": "",
	    "srcAgentAid": "",
	    "srcAgentName": "",
	    "srcChannel": "SIP/8001-00000044",
	    "srcGateway": "",
	    "startTime": 1488785806000,
	    "uniqueid": "1488785806.68"
    }
}
``` 


## 5.13 服务质量评价

- 用途：将客户转到服务质量评价的IVR流程
- 调用时机：Agent在界面上点击“服务质量评价”按钮或链接时调用。
- 效果：正在与Agent通话的客户通话会被转到服务质量评价的语音。

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| sqeSipPhoneName | 坐席的分机号 | String |  | 是 |
| sqeQuestion | 打分请求提示音的路径。注意：不包含扩展名（语音在【IVR语音资源】中上传） | String | | 是 |
| sqeThankyou | 感谢提示音的路径。注意：不包含扩展名（语音在【IVR语音资源】中上传） | String | | 是 |

Request:
```json
{
    "timestamp":1478321775260,
    "command":"sqe",
    "sqeSipPhoneName":"8001",
    "sqeQuestion":"/rsmw/resource/media/longterm/1f50e107339a47b0af7d9a959d0d8cef",
    "sqeThankyou":"/rsmw/resource/media/longterm/da3207be3d16412f812d1ab24a5fdc62"
}
```

## 5.14 判断某号码是否内部exten

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| exten | 需要判断的号码 | String |  | 是 |

Request:
```json
{
    "timestamp":1478321775260,
    "command":"isExten",
    "exten":"8001"
}
```

Response:
```json
{
	"timestamp":1478321775260,
    "errCode": "0",
    "errMsg": "SUCCESS",
    "isExten":true
}
```

## 5.15 监听

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| spySipPhoneName | 监听者所使用的分机 | String |  | 是 |
| spiedSipPhoneName | 被监听的分机 | String |  | 是 |

Request:
```json
{
    "timestamp":1478321775260,
    "command":"chanSpy",
    "spySipPhoneName":"8001",
    "spiedSipPhoneName":"8002"
}
```

## 5.16 语音验证码

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| phoneNumber | 收听语音验证码的手机号 | String |  | 是 |
| gateway | 呼叫phoneNumber所使用的线路或网关设备 | String |  | 是 |
| accessNumber | 呼叫phoneNumber所使用的主叫号码 | String |  | 是 |
| exten | 系统内置语音验证码dialplan的exten，默认9999 | String |  | 是 |
| media1 | 语音1的路径。语音文件在【语音资源】菜单上传。注意，不需要文件扩展名 | String |  | 是 |
| code | 验证码 | String |  | 是 |
| media2 | 语音2的路径。语音文件在【语音资源】菜单上传。注意，不需要文件扩展名 | String |  | 是 |
| repeat  | 重复次数 | String |  | 是 |
| absoluteTimeout | 通话超时时间（秒）。超时后自动挂断 | Integer | 32 | 是 |

Request:
```json
{
        "timestamp":1478321775260,
        "command":"voiceVerificationCode",
        "phoneNumber":"13391026171",
        "gateway":"gw1",
        "accessNumber":"60172133",
        "exten":"9999",
        "media1":"/rsmw/resource/media/longterm/9ead72ceafd4409db11c83cf17ed1dbd",
        "code":"1234",
        "media2":"/rsmw/resource/media/longterm/9ead72ceafd4409db11c83cf17ed1dbd",
        "repeat":3,
        "absoluteTimeout":300
}
```

Response:
```json
{
    "timestamp":1478321775260,
    "errCode":"0",
    "errMsg":"SUCCESS",
    "callUuid":"b37663e3695c41af978ed35a616fdd49"
}
```

## 5.17 【黑名单】增/删/查

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| phonenumber | 号码 | varchar | 64 | 是 |
| typee | 限制类型[BLOCK_IN]禁止呼入 [BLOCK_OUT]禁止呼出 [BLOCK_BOTH]禁止呼入和呼出 | varchar | 16 | 是 |
| description | 描述 | varchar | 512 |  ||


- 【增】

Request:
```json
{
    "timestamp":"1478321782349",
    "command":"blacklistSave",
    "phonenumber":"13391026171",
    "typee":"BLOCK_IN",
    "description":"描述"
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS",
    "id":1
}
```

- 【删】

Request:
```json
{
    "timestamp":"1478321782349",
    "command":"blacklistDelete",
    "idList":[1,2,3]
}
```

Response:
```json
    {
        "timestamp": 1478321782349,
        "errCode": "0",
        "errMsg": "SUCCESS"
    }
```

- 【查】

Request:
```json
    {
        "timestamp":"1478321782349",
        "command":"blacklistGetAll"
    }
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS",
    "result":[
            {
            "id":1,
            "phonenumber":"13391026171",
            "typee":"BLOCK_IN",
            "description":"描述"
            },
            {
            "id":2,
            "phonenumber":"13391026171",
            "typee":"BLOCK_OUT",
            "description":"描述"
            }
        ]
}
```

- 黑名单查询

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| phonenumber | 号码 | varchar | 64 | 是 |

- 【呼出黑名单】

Request:
```json
{
        "timestamp":1478321775260,
        "command":"isPhoneNumberBlockOut",
        "phoneNumber":"13391026171"
}
```

Response:
```json
{
    "timestamp":1478321775260,
    "errCode":"0",
    "errMsg":"SUCCESS",
    "isBlockOut":true
}
```

- 【呼入黑名单】

Request:
```json
{
        "timestamp":1478321775260,
        "command":"isPhoneNumberBlockIn",
        "phoneNumber":"13391026171"
}
```

Response:
```json
{
    "timestamp":1478321775260,
    "errCode":"0",
    "errMsg":"SUCCESS",
    "isBlockIn":true
}
```

## 5.18 电话会议控制

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| exten | 会议室分机号 | String | 64 | 是 |
| phoneNumberList | 号码 | List | 64 | 是 |
| phoneNumber | 号码 | String | 64 |  |
| gateway | 网关 | String | 64 |  |
| accessNumber | 接入号 | String | 64 |  |
| channel | 通话中的channel | String | 64 | 是 |

- 【查询会议的状态】

Request:
```json
{
    "timestamp":"1478321782349",
    "command":"getConferenceStatus",
    "exten":6001
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS",
    "resullt":{
        "isBusy":false,
        "isRecord":false,
        "locked":false,
        "confbridgeListEventMap":{
            "SIP/gw1-00000035":{
                "channel":"SIP/8001-00000035",
                "conference":"6001",
                "conferenceMemberAccessNumber":"",
                "conferenceMemberGateway":"",
                "conferenceMemberLineStates":true,
                "muted":"No",
                "phonenumber":"13391026171"
            }
        },
        "conference":{
            "backgroundColor":"bg-color-blueLight",
            "bridgeMaxmembers":11,
            "bridgeRecordConference":true,
            "conferencePwd":"aaa111",
            "extColumns":{},
            "exten":"6001",
            "id":1,
            "name":"电话会议",
            "userMusicOnHoldClass":"default",
            "userMusiconHoldWhenEmpty":true
        }
    }
}
```

- 【会议室锁定】

Request:
```json
{
    "timestamp":"1478321782349",
    "command":"conferenceLock",
    "exten":6001
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS"
}
```

- 【会议室解锁】

Request:
```json
    {
        "timestamp":"1478321782349",
        "command":"conferenceUnlock",
        "exten":6001
    }
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS"
}
```


- 【邀请】

Request:
```json
{
    "timestamp":1478321775260,
    "command":"conferenceInvite",
    "exten":6001,
    "phoneNumberList":["13391026171","13999999999","18999999999"],
    "gateway":"gateawyName",
    "accessNumber":"66668888"
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS"
}
```

- 【重新邀请】

Request:
```json
{
    "timestamp":1478321775260,
    "command":"conferenceReInvite",
    "exten":6001,
    "phoneNumber":"13391026171"
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS"
}
```

- 【踢出】

Request:
```json
{
    "timestamp":"1478321782349",
    "command":"conferenceKick",
    "exten":6001,
    "channel":"SIP/8001-XXXXXXXX"
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS"
}
```

- 【禁言】

Request:
```json
{
    "timestamp":"1478321782349",
    "command":"conferenceMute",
    "exten":6001,
    "channel":"SIP/8001-XXXXXXXX"
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS"
}
```

- 【解除禁言】

Request:
```json
    {
        "timestamp":"1478321782349",
        "command":"conferenceUnmute",
        "exten":6001,
        "channel":"SIP/8001-XXXXXXXX"
    }
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS"
}
```
# API 配置接口

## 5.19 【接入号】增/删/改/查

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| number | 具体的电话号码，通常是固话或手机 | String | 64 | 是 |
| callin | 此接入号是否允许呼入 | Boolean |  | 是 |
| callout | 此接入号是否允许呼出 | Boolean |  | 是 | 
| id | 保存到数据库后返回的主键 | Boolean |  | 是 |

- 【增】

Request:
```json
{
    "timestamp":1478321775260,
    "command":"accessNumberSave",
    "accessNumber":{
        "number":"66668888",
        "callin":true,
        "callout":false
    }
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS",
    "id":1
}
```

- 【删】

Request:
```json
{
    "timestamp":"1478321782349",
    "command":"accessNumberDelete",
    "idList":[1,2,3]
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS"
}
```

- 【改】

Request:
```json
{
    "timestamp":1478321775260,
    "command":"accessNumberUpdate",
    "accessNumber":{
        "id":1,
        "number":"66668888",
        "callin":true,
        "callout":false
    }
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS"
}
```

- 【查】

Request:
```json
{
    "timestamp":"1478321782349",
    "command":"accessNumberGetAll"
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS",
    "result": [
        {
            "id":1,
            "number":"66668888",
            "callin":true,
            "callout":false
        },
        {
            "id":2,
            "number":"88886666",
            "callin":true,
            "callout":false
        }
    ]
}
```

## 5.20 【网关】增/删/改/查

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| name | 网关名称 | String | 64 | 是 |
| maxChannel | 网关最大并发通道数量 | Integer |  | 是 |
| prefix | 网关前缀，用于外呼时自动加在被叫号码前 | String | 64 | |
| secret | 认证密码，用于SIP Auth | String | 64 | |
| host | 网关IP | String | 64 | |
| port | 网管SIP 端口，默认为5060 | String | 64 | |
| dtmf | dtmf模式,默认RFC2833,固定值[RFC2833,SIPINFO,INBAND] | String | 64 | 是 |
| codec1 | 语音编码，固定值[none,all,alaw,ulaw,g729,g723,g726,g722,gsm,ilbc] | String | 64 | 是 |
| codec2 | 语音编码，固定值[none,all,alaw,ulaw,g729,g723,g726,g722,gsm,ilbc] | String | 64 | 是 |
| codec3 | 语音编码，固定值[none,all,alaw,ulaw,g729,g723,g726,g722,gsm,ilbc] | String | 64 | 是 |
| codec4 | 语音编码，固定值[none,all,alaw,ulaw,g729,g723,g726,g722,gsm,ilbc] | String | 64 | 是 |
| mode | 网关的连接模式,【对接】【向外注册】【向内注册】,固定值[REG_NO,REG_OUT,REG_IN] | String | 64 | 是 |
| regUsername | 注册用户名 | String | 64 |  |
| regAuthusername | 注册鉴权用户名 | String | 64 |  |
| regHost | 注册IP | String | 64 |  |
| regDomain | 注册域名 | String | 64 |  |
| regPort | 注册端口 | String | 64 |  |
| regSecret | 注册密码 | String | 64 |  |
| extension | 绑定号码 | String | 64 |  |
| share | 此网管是否给所有租户共享 | Boolean |  | 是 |


- 【增】

Request:
```json
{
    "timestamp":1478321775260,
    "command":"gatewaySave",
    "gateway":{
        "name":"gw0",
        "maxChannel":30,
        "prefix":"",
        "secret":"pA55w0rd",
        "host":"10.0.0.1",
        "port":"5060",
        "dtmf":"RFC2833",
        "codec1":"PCMA",
        "codec2":"PCMU",
        "codec3":"g729",
        "codec4":"none",
        "mode":"REG_OUT",
        "regUsername":"from your sip provide",
        "regAuthusername":"from your sip provide",
        "regHost":"from your sip provide",
        "regDomain":"from your sip provide",
        "regPort":"5060",
        "regSecret":"from your sip provide",
        "extension":"",
        "share":false
    }
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS",
    "id":1
}
```

- 【删】

Request:
```json
    {
        "timestamp":"1478321782349",
        "command":"gatewayDelete",
        "idList":[1,2,3]
    }
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS"
}
```

- 【改】

Request:
```json
{
    "timestamp":1478321775260,
    "command":"gatewayUpdate",
    "gateway":{
        "id":1,
        "name":"gw0",
        "maxChannel":30,
        "prefix":"",
        "secret":"pA55w0rd",
        "host":"10.0.0.1",
        "port":"5060",
        "dtmf":"RFC2833",
        "codec1":"PCMA",
        "codec2":"PCMU",
        "codec3":"g729",
        "codec4":"none",
        "mode":"REG_OUT",
        "regUsername":"from your sip provide",
        "regAuthusername":"from your sip provide",
        "regHost":"from your sip provide",
        "regDomain":"from your sip provide",
        "regPort":"5060",
        "regSecret":"from your sip provide",
        "extension":"",
        "share":false
    }
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS"
}
```

- 【查】

Request:
```json
{
    "timestamp":"1478321782349",
    "command":"gatewayGetAll"
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS",
    "result": [
        {
            "id":1,
            "name":"gw0",
            "maxChannel":30,
            "prefix":"",
            "secret":"pA55w0rd",
            "host":"10.0.0.1",
            "port":"5060",
            "dtmf":"RFC2833",
            "codec1":"PCMA",
            "codec2":"PCMU",
            "codec3":"g729",
            "codec4":"none",
            "mode":"REG_OUT",
            "regUsername":"from your sip provide",
            "regAuthusername":"from your sip provide",
            "regHost":"from your sip provide",
            "regDomain":"from your sip provide",
            "regPort":"5060",
            "regSecret":"from your sip provide",
            "extension":"",
            "share":false
        },
        {
            "id":2,
            "name":"gw1",
            "maxChannel":30,
            "prefix":"",
            "secret":"pA55w0rd",
            "host":"10.0.0.2",
            "port":"5060",
            "dtmf":"RFC2833",
            "codec1":"PCMA",
            "codec2":"PCMU",
            "codec3":"g729",
            "codec4":"none",
            "mode":"REG_OUT",
            "regUsername":"from your sip provide",
            "regAuthusername":"from your sip provide",
            "regHost":"from your sip provide",
            "regDomain":"from your sip provide",
            "regPort":"5060",
            "regSecret":"from your sip provide",
            "extension":"",
            "share":false
        }        
    ]
}
```

## 5.21 【接入号-网关】关联

Request:
```json
{
    "timestamp":"1478321782349",
    "command":"accessNumberGatewayLink",
    "accessNumberId":1,
    "gatewayIdList":[1,2,3]
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS"
}
```

## 5.22【呼入路由】增/删/改/查

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| seq | 顺序 | Integer |  | 是 |
| name | 名称 | String | 64 | 是 |
| accessNumber | 此条路由规则所应用于此接入号 | String | 64 | 是 |
| startDate | 有效期起始日 | Date |  | 是 |
| endDate | 有效期终止日 | Date |  | 是 |
| periodType | 生效时段类型：[ALL,DAY,DAY_AND_HOUR]，每天生效，具体周几生效，具体周几到几点至几点生效 | Boolean |  | 是 |
| periodDayInWeek | 具体周几生效 | String | 64 |  |
| periodStartTime | 具体周几到几点至几点生效 | String | 64 |  |
| periodEndTime | 具体周几到几点至几点生效 | String | 64 |  |
| destType | 目标类型，固定值[_hangup,SIPPHONE,QUEUE,CONFERENCE,IVR,VM,APIROUTE,USERDIALPLAN,CONTACT,OUTBOUNDNUMBER] | String | 64 | 是 |
| dest | 呼叫目标号码 | String | 64 | |
| dstAccessNumber | 呼叫目标号码所使用到网关 | String | 64 | |
| dstGateway | 呼叫目标号码所使用到接入号 | String | 64 | |
| enableRecentContact | 是否启用“专员路由” | Boolean |  | 是 |
| recentContactDays | “专员路由”所使用的话单数据到天数范围 | Integer |  |  ||

- 【增】

Request:
```json
{
    "timestamp":1478321775260,
    "command":"accessNumberRouteSave",
    "accessNumberRoute":{
        "seq":1,
        "name":"上班时间段转接技能组",
        "accessNumber":"66668888",
        "startDate":"2017-01-01 00:00:00",
        "endDate":"2099-01-01 00:00:00",
        "periodType":"ALL",
        "periodDayInWeek":[1,2,3,4,5,6,7],
        "periodStartTime":"08:00:00",
        "periodEndTime":"18:00:00",
        "destType":"SIPPHONE",
        "dest":"8001",
        "dstAccessNumber":"",
        "dstGateway":"",
        "enableRecentContact":false,
        "recentContactDays":30
    }
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS",
    "id":1
}
```

- 【删】

Request:
```json
{
    "timestamp":"1478321782349",
    "command":"sipPhoneDelete",
    "idList":[1,2,3]
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS"
}
```

- 【改】

Request:
```json
{
    "timestamp":1478321775260,
    "command":"accessNumberRouteUpdate",
    "accessNumberRoute":{
        "id":1,
        "seq":1,
        "name":"上班时间段转接技能组",
        "accessNumber":"66668888",
        "startDate":"2017-01-01 00:00:00",
        "endDate":"2099-01-01 00:00:00",
        "periodType":"ALL",
        "periodDayInWeek":[1,2,3,4,5,6,7],
        "periodStartTime":"08:00:00",
        "periodEndTime":"18:00:00",
        "destType":"SIPPHONE",
        "dest":"8001",
        "dstAccessNumber":"",
        "dstGateway":"",
        "enableRecentContact":false,
        "recentContactDays":30
    }
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS"
}
```

- 【查】

Request:
```json
{
    "timestamp":"1478321782349",
    "command":"accessNumberRouteGetAll"
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS",
    "result": [
        {
            "id":1,
            "seq":1,
            "name":"上班时间段转接技能组",
            "accessNumber":"66668888",
            "startDate":"2017-01-01 00:00:00",
            "endDate":"2099-01-01 00:00:00",
            "periodType":"ALL",
            "periodDayInWeek":[1,2,3,4,5,6,7],
            "periodStartTime":"08:00:00",
            "periodEndTime":"18:00:00",
            "destType":"SIPPHONE",
            "dest":"8001",
            "dstAccessNumber":"",
            "dstGateway":"",
            "enableRecentContact":false,
            "recentContactDays":30
        },
        {
            "id":2,
            "seq":2,
            "name":"下班时间段转接值班手机",
            "accessNumber":"66668888",
            "startDate":"2017-01-01 00:00:00",
            "endDate":"2099-01-01 00:00:00",
            "periodType":"ALL",
            "periodDayInWeek":[1,2,3,4,5,6,7],
            "periodStartTime":"18:00:00",
            "periodEndTime":"22:00:00",
            "destType":"OUTBOUNDNUMBER",
            "dest":"13391026171",
            "dstAccessNumber":"gw0",
            "dstGateway":"66668888",
            "enableRecentContact":false,
            "recentContactDays":30
        }       
    ]
}
```

## 5.23 【技能组】增/删/改/查

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| name | 名称 | String | 64 | 是 |
| exten | 分机号 | String | 64 | 是 |
| musicOnWelcomeQAnnounceId | 欢迎辞id | Long |  | 是 |
| musicClassMohId | 等待音乐id | Long |  | 是 |
| agentAnnounceQAnnounceId | 接通时给坐席播放的提示音id | Long |  | 是 |
| maxLength | 技能组最大排队人数 | Long |  | 是 |
| maxPauseCount | 技能组最大置忙数 | Long |  | 是 |
| serviceLevel | 技能组的“服务等级” | Long |  | 是 |
| strategy | 振铃策略(ACD)，固定值[ringall, leastrecent, fewestcalls, random, rrordered, linear] | String | 64 | 是 |
| agentTimeout | 坐席未接超时 | Long |  | 是 |
| wrapuptime | 坐席冷却时长 | Long |  | 是 |
| autoPause | 是否启用“漏接后自动置忙” | Boolean |  | 是 |
| autoCleanMember | 是否启用“自动清理成员” | Boolean |  | 是 |
| autoCleanMemberTime | “自动清理成员”的时间 | String | 64 |  |
| announceJobNumber | 播工号，固定值[none, exten, jobnumber] | String | 64 | 是 |
| language | 提示音语言，固定值[zh_CN, english] | String | 64 |  |
| queueJobNumberQAnnounceId | “播工号”提示音1 id | Long |  |  |
| queueAtYourServiceQAnnounceId | “播工号”提示音2 id | Long |  |  |
| sqeEnable | 是否启用“服务质量评价” | Boolean |  | 是 |
| sqeQuestionId | “服务质量评价”提示音1 id | Long |  |  |
| sqeThankyouId | “服务质量评价”提示音1 id | Long |  |  |
| enableAnnounce | 是否启用“排队语音提示” | Boolean |  | 是 |
| announceFrequency | “排队语音提示”提示音间隔1 | Long |  |  |
| minAnnounceFrequency | “排队语音提示”提示音间隔2 | Long |  |  |
| queueYouAreNextQAnnounceId | 【您当前排在第一位】提示音id | Long |  |  |
| queueThereAreQAnnounceId | 【目前有】提示音id | Long |  |  |
| queueCallsWatingQAnnounceId | 【个呼叫正在排队】提示音id | Long |  |  |
| queueHoldTimeQAnnounceId | 【等待时长】提示音id | Long |  |  |
| queueMinutesQAnnounceId | 【分】提示音id | Long |  |  |
| queueSecondsQAnnounceId | 【秒】提示音id | Long |  |  |
| queueThankyouQAnnounceId | 【感谢您的耐心等待】提示音id | Long |  |  |
| queueReportholdQAnnounceId | 【播报等待时长】提示音id | Long |  |  |
| queuePeriodicAnnounceQAnnounceId | 【目前座席繁忙，请耐心等待】提示音id | Long |  |  |
| queueTimeout | 技能组超时 | Long |  | 是 |
| destTypeAfterTimeout | 技能组超时后转去的目标类型 | String | 64 |  |
| destAfterTimeout | 技能组超时后转去的目标号码 | String | 64 |  |
| dstAccessNumber | 技能组超时后转去的目标号码所使用的接入号 | String | 64 |  |
| dstGateway | 技能组超时后转去的目标号码所使用的网关 | String | 64 |  |
| hideCallerId | 技能组是否对座席隐藏来电显示 | Boolean |  | 是 ||

            
- 【增】

Request:
```json
{
    "timestamp":1478321775260,
    "command":"queueSave",
    "queue":{
        "name":"客服组",
        "exten":"7001",
        "musicOnWelcomeQAnnounceId":1,
        "musicClassMohId":1,
        "agentAnnounceQAnnounceId":1,
        "maxLength":10,
        "maxPauseCount":10,
        "serviceLevel":15,
        "strategy":"rrordered",
        "agentTimeout":15,
        "wrapuptime":3,
        "autoPause":true,
        "autoCleanMember":false,
        "autoCleanMemberTime":"00:00:00",
        "announceJobNumber":"none",
        "language":"zhCN",
        "queueJobNumberQAnnounceId":1,
        "queueAtYourServiceQAnnounceId":2,
        "sqeEnable":true,
        "sqeQuestionId":1,
        "sqeThankyouId":1,
        "enableAnnounce":true,
        "announceFrequency":1,
        "minAnnounceFrequency":1,
        "queueYouAreNextQAnnounceId":1,
        "queueThereAreQAnnounceId":1,
        "queueCallsWatingQAnnounceId":1,
        "queueHoldTimeQAnnounceId":1,
        "queueMinuteQAnnounceId":1,
        "queueMinutesQAnnounceId":1,
        "queueSecondsQAnnounceId":1,
        "queueThankyouQAnnounceId":1,
        "queueReportholdQAnnounceId":1,
        "queuePeriodicAnnounceQAnnounceId":1,
        "queueTimeout":1,
        "destTypeAfterTimeout":"hangup",
        "destAfterTimeout":"",
        "dstAccessNumber":"",
        "dstGateway":"",
        "hideCallerId":false
    }
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS",
    "id":1
}
```

- 【删】

Request:
```json
{
    "timestamp":"1478321782349",
    "command":"queueDelete",
    "idList":[1,2,3]
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS"
}
```

- 【改】

Request:
```json
{
    "timestamp":1478321775260,
    "command":"queueUpdate",
    "queue":{
        "id":1,
        "name":"客服组",
        "exten":"7001",
        "musicOnWelcomeQAnnounceId":1,
        "musicClassMohId":1,
        "agentAnnounceQAnnounceId":1,
        "maxLength":10,
        "maxPauseCount":10,
        "serviceLevel":15,
        "strategy":"rrordered",
        "agentTimeout":15,
        "wrapuptime":3,
        "autoPause":true,
        "autoCleanMember":false,
        "autoCleanMemberTime":"00:00:00",
        "announceJobNumber":"none",
        "language":"zhCN",
        "queueJobNumberQAnnounceId":1,
        "queueAtYourServiceQAnnounceId":2,
        "sqeEnable":true,
        "sqeQuestionId":1,
        "sqeThankyouId":1,
        "enableAnnounce":true,
        "announceFrequency":1,
        "minAnnounceFrequency":1,
        "queueYouAreNextQAnnounceId":1,
        "queueThereAreQAnnounceId":1,
        "queueCallsWatingQAnnounceId":1,
        "queueHoldTimeQAnnounceId":1,
        "queueMinuteQAnnounceId":1,
        "queueMinutesQAnnounceId":1,
        "queueSecondsQAnnounceId":1,
        "queueThankyouQAnnounceId":1,
        "queueReportholdQAnnounceId":1,
        "queuePeriodicAnnounceQAnnounceId":1,
        "queueTimeout":1,
        "destTypeAfterTimeout":"hangup",
        "destAfterTimeout":"",
        "dstAccessNumber":"",
        "dstGateway":"",
        "hideCallerId":false
    }
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS"
}
```

- 【查】

Request:
```json
{
    "timestamp":"1478321782349",
    "command":"queueGetAll"
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS",
    "result": [
        {
            "id":1,
            "name":"客服组",
            "exten":"7001",
            "musicOnWelcomeQAnnounceId":1,
            "musicClassMohId":1,
            "agentAnnounceQAnnounceId":1,
            "maxLength":10,
            "maxPauseCount":10,
            "serviceLevel":15,
            "strategy":"rrordered",
            "agentTimeout":15,
            "wrapuptime":3,
            "autoPause":true,
            "autoCleanMember":false,
            "autoCleanMemberTime":"00:00:00",
            "announceJobNumber":"none",
            "language":"zhCN",
            "queueJobNumberQAnnounceId":1,
            "queueAtYourServiceQAnnounceId":2,
            "sqeEnable":true,
            "sqeQuestionId":1,
            "sqeThankyouId":1,
            "enableAnnounce":true,
            "announceFrequency":1,
            "minAnnounceFrequency":1,
            "queueYouAreNextQAnnounceId":1,
            "queueThereAreQAnnounceId":1,
            "queueCallsWatingQAnnounceId":1,
            "queueHoldTimeQAnnounceId":1,
            "queueMinuteQAnnounceId":1,
            "queueMinutesQAnnounceId":1,
            "queueSecondsQAnnounceId":1,
            "queueThankyouQAnnounceId":1,
            "queueReportholdQAnnounceId":1,
            "queuePeriodicAnnounceQAnnounceId":1,
            "queueTimeout":1,
            "destTypeAfterTimeout":"hangup",
            "destAfterTimeout":"",
            "dstAccessNumber":"",
            "dstGateway":"",
            "hideCallerId":false
        }   
    ]
}
```

## 5.24 【技能组成员-手机】增/删

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| queueId | 技能组id | Long |  | 是 |
| outboundNumber | 座席手机号 | String | 64 | 是 |
| gatewayName | 呼叫座席手机所用的线路 | String | 64 | 是 |
| penalty | 优先级0~10，0最高，10最低 | Integer |  |  ||

- 【增】

Request:
```json
{
    "timestamp":"1478321782349",
    "command":"queueMemberSaveOutboundNumber",
    "queueId":1,
    "outboundNumber":"13391026171",
    "gatewayName":"gw0",
    "penalty":5
}
```
Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS",
    "id":1
}
```

- 【删】

Request:
```json
    {
        "timestamp":"1478321782349",
        "command":"queueMemberDeleteOutboundNumber",
        "queueId":1,
        "outboundNumberId":1
    }
```
Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS"
}
```


## 5.25 【技能组成员-SIP分机和座席】关联

| 字段 | 说明 | 类型 | 长度 | 必填 |
| ---- | ---- | :--: | :--: | :--: |
| queueId | 技能组id | Long |  | 是 |
| sipPhoneIdList | 座席SIP分机id | String | 64 | 是 |
| agentAidList | 座席aid | String | 64 | 是 ||


Request:
```json
{
    "timestamp":"1478321782349",
    "command":"queueMemberSetSipPhoneOrAgent",
    "queueId":1,
    "sipPhoneIdList":[1,2,3],
    "agentAidList":["aid1", "aid2"]
}
```

Response:
```json
{
    "timestamp": 1478321782349,
    "errCode": "0",
    "errMsg": "SUCCESS"
}
```
