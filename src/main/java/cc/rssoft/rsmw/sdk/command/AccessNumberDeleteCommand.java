package cc.rssoft.rsmw.sdk.command;

import java.util.ArrayList;
import java.util.List;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class AccessNumberDeleteCommand extends AbstractApiCommand {

	private String command = "accessNumberDelete";

	private List<Long> idList = new ArrayList<>();

	public AccessNumberDeleteCommand(List<Long> idList) {
		this.setIdList(idList);
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public List<Long> getIdList() {
		return idList;
	}

	public void setIdList(List<Long> idList) {
		this.idList = idList;
	}
	
	

}
