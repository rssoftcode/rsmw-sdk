package cc.rssoft.rsmw.sdk.command.bean;

import java.util.LinkedHashMap;
import java.util.Map;

public class Conference {

	private Long id = null;
	
	// 背景色
	private String backgroundColor = null;
	
	// 最大成员数
	private Long bridgeMaxmembers = null;
	
	// 是否录音
	private Boolean bridgeRecordConference = null;
	
	//会议室密码
	private String conferencePwd = null;
	
	//扩展字段
	private Map<String, Object> extColumns = new LinkedHashMap<>();
	
	// 会议室exten，用于刷配置文件和meetme()
	private String exten = null;
	
	// 会议室中文名
	private String name = null;
	
	private Boolean isSipphoneConference = null;
	
	// 是否开启背景音乐
	private Boolean userMusiconHoldWhenEmpty;
	
	// 背景音乐
	private String userMusicOnHoldClass = "";

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public Long getBridgeMaxmembers() {
		return bridgeMaxmembers;
	}

	public void setBridgeMaxmembers(Long bridgeMaxmembers) {
		this.bridgeMaxmembers = bridgeMaxmembers;
	}

	public Boolean getBridgeRecordConference() {
		return bridgeRecordConference;
	}

	public void setBridgeRecordConference(Boolean bridgeRecordConference) {
		this.bridgeRecordConference = bridgeRecordConference;
	}

	public String getConferencePwd() {
		return conferencePwd;
	}

	public void setConferencePwd(String conferencePwd) {
		this.conferencePwd = conferencePwd;
	}

	public Map<String, Object> getExtColumns() {
		return extColumns;
	}

	public void setExtColumns(Map<String, Object> extColumns) {
		this.extColumns = extColumns;
	}

	public String getExten() {
		return exten;
	}

	public void setExten(String exten) {
		this.exten = exten;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getIsSipphoneConference() {
		return isSipphoneConference;
	}

	public void setIsSipphoneConference(Boolean isSipphoneConference) {
		this.isSipphoneConference = isSipphoneConference;
	}

	public Boolean getUserMusiconHoldWhenEmpty() {
		return userMusiconHoldWhenEmpty;
	}

	public void setUserMusiconHoldWhenEmpty(Boolean userMusiconHoldWhenEmpty) {
		this.userMusiconHoldWhenEmpty = userMusiconHoldWhenEmpty;
	}

	public String getUserMusicOnHoldClass() {
		return userMusicOnHoldClass;
	}

	public void setUserMusicOnHoldClass(String userMusicOnHoldClass) {
		this.userMusicOnHoldClass = userMusicOnHoldClass;
	}
	
}
