package cc.rssoft.rsmw.sdk.command.response;

import java.util.Map;

import cc.rssoft.rsmw.sdk.command.bean.GatewayStatus;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;

public class GetGatewayStatusResponse extends AbstractApiResponse {

	private Map<String, GatewayStatus> result = null;

	/**
	 * key : sipPhoneName。即sipPhone的分机号<br>
	 * value : SipPhoneStatus。即分机状态信息<br>
	 * @return result
	 */
	public Map<String, GatewayStatus> getResult() {
		return result;
	}

	public void setResult(Map<String, GatewayStatus> result) {
		this.result = result;
	}

}
