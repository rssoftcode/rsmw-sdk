package cc.rssoft.rsmw.sdk.command.bean;

/**
 * 
 * 网关
 * 
 * @author jiang
 *
 */

public class Gateway  {
	
	private Long id = null;
	
	/**
	 * 
	 * 只支持英文和数字，不可以用中文！
	 * 
	 * 名称
	 * 
	 */
	private String name = null;
	
	/**
	 * 此网关最大通道数，用于控制呼出（达到最大通道数后，不再向此网关发送数据）
	 */
	private Integer maxChannel = null;
	
	/**
	 * 前缀，呼出时自动加载被叫号码的头部
	 */
	private String prefix = null;
	
	/**
	 * 
	 * 只支持英文和数字，不可以用中文！
	 * 
	 * 密码
	 * 
	 */
	private String secret = null;

	/**
	 * host （SIP 代理 IP）
	 */
	private String host = null;
	
	/**
	 * 端口（SIP 代理 端口）
	 */	
	private String port = null;
	
	/**
	 * DTMF
	 */
	private String dtmf = null;
	
	/**
	 * 编码1
	 */
	private String codec1 = null;
	/**
	 * 编码2
	 */
	private String codec2 = null;
	/**
	 * 编码3
	 */
	private String codec3 = null;
	/**
	 * 编码4
	 */
	private String codec4 = null;

	/**
	 * 该字段在form中是一个checkbox，勾选后，显示regXXX字段
	 * 向外注册，向内注册，对接
	 */
	private String mode = null;
	/**
	 * 用户名
	 */	
	private String regUsername = null;
	/**
	 * 鉴权用户名
	 */
	private String regAuthusername = null;	
	/**
	 * IP
	 */
	private String regHost = null;
	/**
	 * 域
	 */
	private String regDomain = null;
	/**
	 * 端口
	 */
	private String regPort = null;
	/**
	 * 密码
	 */
	private String regSecret = null;

	/**
	 * 注册字符串中的extension，用来在呼入时对方平台没有送被叫号码时可以匹配到一个明确的号码，而不是s分机
	 */
	private String extension = null;

	private Boolean share = null;
	
	public String getRegUsername() {
		return regUsername;
	}

	public void setRegUsername(String regUsername) {
		this.regUsername = regUsername;
	}

	public String getRegDomain() {
		return regDomain;
	}

	public void setRegDomain(String regDomain) {
		this.regDomain = regDomain;
	}

	public String getRegSecret() {
		return regSecret;
	}

	public void setRegSecret(String regSecret) {
		this.regSecret = regSecret;
	}

	public String getRegAuthusername() {
		return regAuthusername;
	}

	public void setRegAuthusername(String regAuthusername) {
		this.regAuthusername = regAuthusername;
	}

	public String getRegHost() {
		return regHost;
	}

	public void setRegHost(String regHost) {
		this.regHost = regHost;
	}

	public String getRegPort() {
		return regPort;
	}

	public void setRegPort(String regPort) {
		this.regPort = regPort;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getDtmf() {
		return dtmf;
	}

	public void setDtmf(String dtmf) {
		this.dtmf = dtmf;
	}

	public String getCodec1() {
		return codec1;
	}

	public void setCodec1(String codec1) {
		this.codec1 = codec1;
	}

	public String getCodec2() {
		return codec2;
	}

	public void setCodec2(String codec2) {
		this.codec2 = codec2;
	}

	public String getCodec3() {
		return codec3;
	}

	public void setCodec3(String codec3) {
		this.codec3 = codec3;
	}

	public String getCodec4() {
		return codec4;
	}

	public void setCodec4(String codec4) {
		this.codec4 = codec4;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public Integer getMaxChannel() {
		return maxChannel;
	}

	public void setMaxChannel(Integer maxChannel) {
		this.maxChannel = maxChannel;
	}

	public Boolean getShare() {
		return share;
	}

	public void setShare(Boolean share) {
		this.share = share;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
}
