package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class TenantActionNodeTreeGetCommand extends AbstractApiCommand {

	private String command = "tenantActionNodeTreeGet";

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

}
