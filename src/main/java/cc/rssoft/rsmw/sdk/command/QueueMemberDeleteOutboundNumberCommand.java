package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class QueueMemberDeleteOutboundNumberCommand extends AbstractApiCommand {

	private String command = "queueMemberDeleteOutboundNumber";

	private Long queueId = null;
	private Long outboundNumberId = null;

	public QueueMemberDeleteOutboundNumberCommand(Long queueId, Long outboundNumberId) {
		setQueueId(queueId);
		setOutboundNumberId(outboundNumberId);
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public Long getQueueId() {
		return queueId;
	}

	public void setQueueId(Long queueId) {
		this.queueId = queueId;
	}

	public Long getOutboundNumberId() {
		return outboundNumberId;
	}

	public void setOutboundNumberId(Long outboundNumberId) {
		this.outboundNumberId = outboundNumberId;
	}

}
