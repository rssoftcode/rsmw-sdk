package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class QueueMemberSaveOutboundNumberCommand extends AbstractApiCommand {

	private String command = "queueMemberSaveOutboundNumber";

	private Long queueId = null;
	private String outboundNumber = null;
	private String gatewayName = null;
	private Integer penalty = null;

	public QueueMemberSaveOutboundNumberCommand(Long queueId, String outboundNumber, String gatewayName, Integer penalty) {
		setQueueId(queueId);
		setOutboundNumber(outboundNumber);
		setGatewayName(gatewayName);
		setPenalty(penalty);
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public Long getQueueId() {
		return queueId;
	}

	public void setQueueId(Long queueId) {
		this.queueId = queueId;
	}

	public String getOutboundNumber() {
		return outboundNumber;
	}

	public void setOutboundNumber(String outboundNumber) {
		this.outboundNumber = outboundNumber;
	}

	public String getGatewayName() {
		return gatewayName;
	}

	public void setGatewayName(String gatewayName) {
		this.gatewayName = gatewayName;
	}

	public Integer getPenalty() {
		return penalty;
	}

	public void setPenalty(Integer penalty) {
		this.penalty = penalty;
	}

}
