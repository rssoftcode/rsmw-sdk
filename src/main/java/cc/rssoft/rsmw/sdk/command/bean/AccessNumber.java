package cc.rssoft.rsmw.sdk.command.bean;

public class AccessNumber {
	/**
	 * ID
	 */
	private Long id = null;

	/**
	 * 接入号
	 */
	private String number = "";
	
	/**
	 * 允许呼入
	 */
	private Boolean callin = null;
	
	/**
	 * 允许呼出
	 */
	private Boolean callout = null;
		
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Boolean getCallin() {
		return callin;
	}

	public void setCallin(Boolean callin) {
		this.callin = callin;
	}

	public Boolean getCallout() {
		return callout;
	}

	public void setCallout(Boolean callout) {
		this.callout = callout;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
