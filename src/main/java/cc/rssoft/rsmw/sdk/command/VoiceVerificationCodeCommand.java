package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class VoiceVerificationCodeCommand extends AbstractApiCommand {

	private String command = "voiceVerificationCode";

	private String phoneNumber = "";
	private String gateway = "";
	private String accessNumber = "";
	private String exten = "";
	private String media1 = "";
	private String code = "";
	private String media2 = "";
	private Integer repeat = 3;
	private Integer absoluteTimeout = 0;

	public VoiceVerificationCodeCommand(String phoneNumber, String gateway, String accessNumber, String exten,
			String media1, String code, String media2, Integer repeat, Integer absoluteTimeout) {
		setPhoneNumber(phoneNumber);
		setGateway(gateway);
		setAccessNumber(accessNumber);
		setExten(exten);
		setMedia1(media1);
		setCode(code);
		setMedia2(media2);
		setRepeat(repeat);
		setAbsoluteTimeout(absoluteTimeout);
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public Integer getAbsoluteTimeout() {
		return absoluteTimeout;
	}

	public void setAbsoluteTimeout(Integer absoluteTimeout) {
		this.absoluteTimeout = absoluteTimeout;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public String getAccessNumber() {
		return accessNumber;
	}

	public void setAccessNumber(String accessNumber) {
		this.accessNumber = accessNumber;
	}

	public String getExten() {
		return exten;
	}

	public void setExten(String exten) {
		this.exten = exten;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getRepeat() {
		return repeat;
	}

	public void setRepeat(Integer repeat) {
		this.repeat = repeat;
	}

	public String getMedia1() {
		return media1;
	}

	public void setMedia1(String media1) {
		this.media1 = media1;
	}

	public String getMedia2() {
		return media2;
	}

	public void setMedia2(String media2) {
		this.media2 = media2;
	}

}
