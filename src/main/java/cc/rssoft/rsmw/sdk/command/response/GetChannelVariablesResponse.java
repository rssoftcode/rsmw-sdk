package cc.rssoft.rsmw.sdk.command.response;

import java.util.Map;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;

public class GetChannelVariablesResponse extends AbstractApiResponse {

	private Map<String, Map<String, String>> result = null;

	/**
	 * key : sipPhoneName。即sipPhone的分机号<br>
	 * value : SipPhoneStatus。即分机状态信息<br>
	 * @return result
	 */
	public Map<String, Map<String, String>> getResult() {
		return result;
	}

	public void setResult(Map<String, Map<String, String>> result) {
		this.result = result;
	}

}
