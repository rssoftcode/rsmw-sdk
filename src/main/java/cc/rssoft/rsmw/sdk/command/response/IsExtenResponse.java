package cc.rssoft.rsmw.sdk.command.response;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;

public class IsExtenResponse extends AbstractApiResponse{
	
	private Boolean isExten = null;

	public Boolean getIsExten() {
		return isExten;
	}

	public void setIsExten(Boolean isExten) {
		this.isExten = isExten;
	}
	
}
