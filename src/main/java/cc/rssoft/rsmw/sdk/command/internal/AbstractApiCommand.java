package cc.rssoft.rsmw.sdk.command.internal;

import com.alibaba.fastjson.JSON;

public abstract class AbstractApiCommand {
	
	
	private Long timestamp = System.currentTimeMillis();

	/**
	 * 自1970年1月1日0时起的“毫秒数”<br>
	 * 调用者服务器的时间和RSMW服务器的时间误差不能超过10秒。否则系统会认为时间戳失效。
	 * @return timestamp
	 */
	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * 将当前对象转换成JSON格式的字符串
	 * @return jsonString
	 */
	public String toJsonString(){
		return JSON.toJSONString(this);
	}
	
}
