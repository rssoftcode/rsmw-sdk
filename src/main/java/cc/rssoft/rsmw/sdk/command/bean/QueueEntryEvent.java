
package cc.rssoft.rsmw.sdk.command.bean;


public class QueueEntryEvent
{

    private String queue;
    private Integer position;
    private String uniqueId;
    private String channel;
    private String callerId;
    private String callerIdName;
    private String callerIdNum;
    private String connectedlinename;
    private String connectedlinenum;
    private Long wait;

    /**
     * Returns the name of the queue that contains this entry.
     */
    public String getQueue()
    {
        return queue;
    }

    /**
     * Sets the name of the queue that contains this entry.
     */
    public void setQueue(String queue)
    {
        this.queue = queue;
    }

    /**
     * Returns the position of this entry in the queue.
     */
    public Integer getPosition()
    {
        return position;
    }

    /**
     * Sets the position of this entry in the queue.
     */
    public void setPosition(Integer position)
    {
        this.position = position;
    }

    /**
     * Returns the name of the channel of this entry.
     */
    public String getChannel()
    {
        return channel;
    }

    /**
     * Returns the unique id of the channel of this entry.<p>
     * Available since Asterisk 1.6.
     *
     * @return the unique id of the channel of this entry.
     * @since 1.0.0
     */
    public String getUniqueId()
    {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId)
    {
        this.uniqueId = uniqueId;
    }

    /**
     * Sets the name of the channel of this entry.
     *
     * @param channel the name of the channel of this entry.
     */
    public void setChannel(String channel)
    {
        this.channel = channel;
    }

	/**
     * Returns the the Caller*ID number of this entry.
     *
     * @return the the Caller*ID number of this entry.
     */
    public String getCallerId() 
    {
		return callerId;
	}

    /**
     * Sets the the Caller*ID number of this entry.
     *
     * @param callerId the the Caller*ID number of this entry.
     */
	public void setCallerId(String callerId) 
	{
		this.callerId = callerId;
	}

    /**
     * Returns the Caller*ID name of this entry.
     *
     * @return the Caller*ID name of this entry.
     * @since 0.2
     */
    public String getCallerIdName()
    {
        return callerIdName;
    }

    /**
     * Sets the Caller*ID name of this entry.
     *
     * @param callerIdName the Caller*ID name of this entry.
     * @since 0.2
     */
    public void setCallerIdName(String callerIdName)
    {
        this.callerIdName = callerIdName;
    }

    /**
     * Gets the Caller*ID num of this entry.
     *
     * @return the Caller*ID num of this entry.
     * @since 1.0.0
     */
    public String getCallerIdNum()
    {
        return callerIdNum;
    }

    /**
     * Sets the Caller*ID num of this entry.
     *
     * @param callerIdNum the Caller*ID num of this entry.
     * @since 1.0.0
     */
    public void setCallerIdNum(String callerIdNum)
    {
        this.callerIdNum = callerIdNum;
    }

    /**
     * Returns the number of seconds this entry has spent in the queue.
     */
    public Long getWait()
    {
        return wait;
    }

    /**
     * Sets the number of seconds this entry has spent in the queue.
     */
    public void setWait(Long wait)
    {
        this.wait = wait;
    }

	public String getConnectedlinename() {
		return connectedlinename;
	}

	public void setConnectedlinename(String connectedlinename) {
		this.connectedlinename = connectedlinename;
	}

	public String getConnectedlinenum() {
		return connectedlinenum;
	}

	public void setConnectedlinenum(String connectedlinenum) {
		this.connectedlinenum = connectedlinenum;
	}
}
