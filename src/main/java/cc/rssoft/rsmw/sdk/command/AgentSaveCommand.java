package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.bean.Agent;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class AgentSaveCommand extends AbstractApiCommand {

	private String command = "agentSave";

	private Agent agent = null;

	/**
	 * @param agentAid
	 * @param agentName
	 * @param agentJobNumber
	 */
	public AgentSaveCommand(Agent agent) {
	    this.agent = agent;

	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

}
