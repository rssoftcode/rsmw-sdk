package cc.rssoft.rsmw.sdk.command;

import java.util.ArrayList;
import java.util.List;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class QueueMemberSetSipPhoneOrAgentCommand extends AbstractApiCommand {

	private String command = "queueMemberSetSipPhoneOrAgent";

	private Long queueId = null;
	private List<Long> sipPhoneIdList = new ArrayList<>();
	private List<String> agentAidList = new ArrayList<>();

	public QueueMemberSetSipPhoneOrAgentCommand(Long queueId, List<Long> sipPhoneIdList, List<String> agentAidList) {
		setQueueId(queueId);
		setSipPhoneIdList(sipPhoneIdList);
		setAgentAidList(agentAidList);
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public List<String> getAgentAidList() {
		return agentAidList;
	}

	public void setAgentAidList(List<String> agentAidList) {
		this.agentAidList = agentAidList;
	}

	public List<Long> getSipPhoneIdList() {
		return sipPhoneIdList;
	}

	public void setSipPhoneIdList(List<Long> sipPhoneIdList) {
		this.sipPhoneIdList = sipPhoneIdList;
	}

	public Long getQueueId() {
		return queueId;
	}

	public void setQueueId(Long queueId) {
		this.queueId = queueId;
	}

}
