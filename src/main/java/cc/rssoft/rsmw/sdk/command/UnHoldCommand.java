package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class UnHoldCommand extends AbstractApiCommand {

	private String command = "unhold";

	private String sipPhoneName = "";

	/**
	 * @param sipPhoneName
	 */
	public UnHoldCommand(String sipPhoneName) {
		this.sipPhoneName = sipPhoneName;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getSipPhoneName() {
		return sipPhoneName;
	}

	public void setSipPhoneName(String sipPhoneName) {
		this.sipPhoneName = sipPhoneName;
	}
	

}
