package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.bean.AccessNumber;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class AccessNumberSaveCommand extends AbstractApiCommand {

	private String command = "accessNumberSave";

	private AccessNumber accessNumber = null;

	public AccessNumberSaveCommand(AccessNumber accessNumber) {
	    this.setAccessNumber(accessNumber);
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public AccessNumber getAccessNumber() {
		return accessNumber;
	}

	public void setAccessNumber(AccessNumber accessNumber) {
		this.accessNumber = accessNumber;
	}



}
