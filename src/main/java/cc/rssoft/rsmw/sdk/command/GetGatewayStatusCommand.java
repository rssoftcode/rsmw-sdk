package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class GetGatewayStatusCommand extends AbstractApiCommand {
	
	private String command = "getGatewayStatus";

	public GetGatewayStatusCommand(){
		
	}
	
	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

}
