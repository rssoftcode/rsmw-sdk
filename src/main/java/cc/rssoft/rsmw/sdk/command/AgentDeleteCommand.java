package cc.rssoft.rsmw.sdk.command;

import java.util.ArrayList;
import java.util.List;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class AgentDeleteCommand extends AbstractApiCommand {

	private String command = "agentDelete";

	private List<String> agentAidList = new ArrayList<>();

	public AgentDeleteCommand(List<String> agentAidList) {
		this.agentAidList = agentAidList;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public List<String> getAgentAidList() {
		return agentAidList;
	}

	public void setAgentAidList(List<String> agentAidList) {
		this.agentAidList = agentAidList;
	}
	
	

}
