package cc.rssoft.rsmw.sdk.command;

import java.util.ArrayList;
import java.util.List;

import cc.rssoft.rsmw.sdk.command.bean.Agent;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class AgentSyncCommand extends AbstractApiCommand {

	private String command = "agentSync";

	private List<Agent> agentList = new ArrayList<>();

	/**
	 * @param agentAid
	 * @param agentName
	 * @param agentJobNumber
	 */
	public AgentSyncCommand(List<Agent> agentList) {
		this.agentList = agentList;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public List<Agent> getAgentList() {
		return agentList;
	}

	public void setAgentList(List<Agent> agentList) {
		this.agentList = agentList;
	}

}
