package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.bean.Gateway;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class GatewaySaveCommand extends AbstractApiCommand {

	private String command = "gatewaySave";

	private Gateway gateway = null;

	public GatewaySaveCommand(Gateway gateway) {
	    this.setGateway(gateway);
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public Gateway getGateway() {
		return gateway;
	}

	public void setGateway(Gateway gateway) {
		this.gateway = gateway;
	}



}
