package cc.rssoft.rsmw.sdk.command.response;

import java.util.List;

import cc.rssoft.rsmw.sdk.command.bean.Blacklist;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;

public class BlacklistGetAllResponse extends AbstractApiResponse {

	private List<Blacklist> result = null;

	public List<Blacklist> getResult() {
		return result;
	}

	public void setResult(List<Blacklist> result) {
		this.result = result;
	}


}
