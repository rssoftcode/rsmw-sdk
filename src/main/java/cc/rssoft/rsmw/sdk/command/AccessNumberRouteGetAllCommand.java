package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class AccessNumberRouteGetAllCommand extends AbstractApiCommand {

	private String command = "accessNumberRouteGetAll";

	public AccessNumberRouteGetAllCommand() {
		
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

}
