package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class ConferenceKickCommand extends AbstractApiCommand {

	private String command = "conferenceKick";

	private String exten = null;

	private String channel = null;

	public ConferenceKickCommand(String exten, String channel) {
		this.exten = exten;
		this.channel = channel;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getExten() {
		return exten;
	}

	public void setExten(String exten) {
		this.exten = exten;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

}
