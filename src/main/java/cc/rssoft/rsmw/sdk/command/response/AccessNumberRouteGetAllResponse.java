package cc.rssoft.rsmw.sdk.command.response;

import java.util.List;

import cc.rssoft.rsmw.sdk.command.bean.AccessNumberRoute;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;

public class AccessNumberRouteGetAllResponse extends AbstractApiResponse {

	private List<AccessNumberRoute> result = null;

	public List<AccessNumberRoute> getResult() {
		return result;
	}

	public void setResult(List<AccessNumberRoute> result) {
		this.result = result;
	}

}
