package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class QueueMemberPauseCommand extends AbstractApiCommand {

	private String command = "queueMemberPause";
	private String queueMemberInterface = "";
	private String reason = "";

	/**
	 * @param queueMemberInterface
	 */
	public QueueMemberPauseCommand(String queueMemberInterface) {
		this.queueMemberInterface = queueMemberInterface;
	}

	/**
	 * @param queueMemberInterface
	 * @param reason
	 */
	public QueueMemberPauseCommand(String queueMemberInterface, String reason) {
		this.queueMemberInterface = queueMemberInterface;
		this.reason = reason;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getQueueMemberInterface() {
		return queueMemberInterface;
	}

	public void setQueueMemberInterface(String queueMemberInterface) {
		this.queueMemberInterface = queueMemberInterface;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
