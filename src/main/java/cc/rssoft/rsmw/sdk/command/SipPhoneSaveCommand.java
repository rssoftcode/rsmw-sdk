package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.bean.SipPhone;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class SipPhoneSaveCommand extends AbstractApiCommand {

	private String command = "sipPhoneSave";

	private SipPhone sipPhone = null;

	public SipPhoneSaveCommand(SipPhone sipPhone) {
	    this.setSipPhone(sipPhone);
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public SipPhone getSipPhone() {
		return sipPhone;
	}

	public void setSipPhone(SipPhone sipPhone) {
		this.sipPhone = sipPhone;
	}

}
