package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.bean.Queue;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class QueueUpdateCommand extends AbstractApiCommand {

	private String command = "queueUpdate";

	private Queue queue = null;

	public QueueUpdateCommand(Queue queue) {
	    this.setQueue(queue);
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}


	public Queue getQueue() {
		return queue;
	}

	public void setQueue(Queue queue) {
		this.queue = queue;
	}



}
