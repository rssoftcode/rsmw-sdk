package cc.rssoft.rsmw.sdk.command.response;

import java.util.Map;

import cc.rssoft.rsmw.sdk.command.bean.Agent;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;

public class AgentGetAllResponse extends AbstractApiResponse {

	private Map<String, Agent> result = null;

    public Map<String, Agent> getResult() {
        return result;
    }

    public void setResult(Map<String, Agent> result) {
        this.result = result;
    }


}
