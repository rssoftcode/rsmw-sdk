package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class GetCdrByUniqueidCommand extends AbstractApiCommand {

	private String command = "getCdrByUniqueid";
	
	private String uniqueid = "";

	public GetCdrByUniqueidCommand(String uniqueid) {
		this.uniqueid = uniqueid;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getUniqueid() {
		return uniqueid;
	}

	public void setUniqueid(String uniqueid) {
		this.uniqueid = uniqueid;
	}

}
