package cc.rssoft.rsmw.sdk.command;

import java.util.ArrayList;
import java.util.List;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class BlacklistDeleteCommand extends AbstractApiCommand {

	private String command = "blacklistDelete";

	private List<Long> idList = new ArrayList<>();

	public BlacklistDeleteCommand(List<Long> idList) {
		this.setIdList(idList);
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

    public List<Long> getIdList() {
        return idList;
    }

    public void setIdList(List<Long> idList) {
        this.idList = idList;
    }


	
	

}
