package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class AccessNumberGetAllCommand extends AbstractApiCommand {

	private String command = "accessNumberGetAll";

	public AccessNumberGetAllCommand() {
		
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

}
