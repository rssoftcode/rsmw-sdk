package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class GetQueueStatusCommand extends AbstractApiCommand {
	
	private String command = "getQueueStatus";

	public GetQueueStatusCommand(){
		
	}
	
	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

}
