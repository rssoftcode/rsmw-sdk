
package cc.rssoft.rsmw.sdk.command.bean;

/**
 */
public class QueueMemberBean {

	private String queue;
	private String location;
	private String membership;
	private String name;
	private Integer penalty;
	private Integer callsTaken;
	private Long lastCall;
	private Integer status;
	private Boolean paused;
	private String stateinterface;
	private Integer isincall;
	
	private String actionId;
   private String internalActionId;

	public String getQueue() {
		return queue;
	}

	public void setQueue(String queue) {
		this.queue = queue;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getMembership() {
		return membership;
	}

	public void setMembership(String membership) {
		this.membership = membership;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPenalty() {
		return penalty;
	}

	public void setPenalty(Integer penalty) {
		this.penalty = penalty;
	}

	public Integer getCallsTaken() {
		return callsTaken;
	}

	public void setCallsTaken(Integer callsTaken) {
		this.callsTaken = callsTaken;
	}

	public Long getLastCall() {
		return lastCall;
	}

	public void setLastCall(Long lastCall) {
		this.lastCall = lastCall;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Boolean getPaused() {
		return paused;
	}

	public void setPaused(Boolean paused) {
		this.paused = paused;
	}

	public String getStateinterface() {
		return stateinterface;
	}

	public void setStateinterface(String stateinterface) {
		this.stateinterface = stateinterface;
	}

	public Integer getIsincall() {
		return isincall;
	}

	public void setIsincall(Integer isincall) {
		this.isincall = isincall;
	}

	public String getActionId() {
		return actionId;
	}

	public void setActionId(String actionId) {
		this.actionId = actionId;
	}

	public String getInternalActionId() {
		return internalActionId;
	}

	public void setInternalActionId(String internalActionId) {
		this.internalActionId = internalActionId;
	}


}
