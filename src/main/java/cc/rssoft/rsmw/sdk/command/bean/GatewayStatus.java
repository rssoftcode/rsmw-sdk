package cc.rssoft.rsmw.sdk.command.bean;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

public class GatewayStatus {

	private Gateway gateway = null;
	private String gatewayName = "";
	private String gatewayPrefix = "";

	private Boolean online = false;
	private String status = "";
	private GatewayRegistryStatus registyStatus = null;

	// key=channel, value=event
	private Map<String, ChannelBean> channels = new ConcurrentHashMap<>();

	private List<Integer> historyChannelCount = new CopyOnWriteArrayList<>();
	
	public String getGatewayName() {
		return gatewayName;
	}

	public void setGatewayName(String gatewayName) {
		this.gatewayName = gatewayName;
	}

	public Boolean getOnline() {
		return online;
	}

	public void setOnline(Boolean online) {
		this.online = online;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Map<String, ChannelBean> getChannels() {
		return channels;
	}

	public void setChannels(Map<String, ChannelBean> channels) {
		this.channels = channels;
	}

	public String getGatewayPrefix() {
		return gatewayPrefix;
	}

	public void setGatewayPrefix(String gatewayPrefix) {
		this.gatewayPrefix = gatewayPrefix;
	}

	public List<Integer> getHistoryChannelCount() {
		return historyChannelCount;
	}

	public void setHistoryChannelCount(List<Integer> historyChannelCount) {
		this.historyChannelCount = historyChannelCount;
	}

	public GatewayRegistryStatus getRegistyStatus() {
		return registyStatus;
	}

	public void setRegistyStatus(GatewayRegistryStatus registyStatus) {
		this.registyStatus = registyStatus;
	}

	public Gateway getGateway() {
		return gateway;
	}

	public void setGateway(Gateway gateway) {
		this.gateway = gateway;
	}

}
