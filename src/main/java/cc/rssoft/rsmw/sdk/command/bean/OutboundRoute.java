package cc.rssoft.rsmw.sdk.command.bean;

public class OutboundRoute {
	
	private String dstString = ""; //88xxx
	private String gatewayName = "";
	private String accessNumber = "";
	
	public String getDstString() {
		return dstString;
	}
	public void setDstString(String dstString) {
		this.dstString = dstString;
	}

	public String getAccessNumber() {
		return accessNumber;
	}
	public void setAccessNumber(String accessNumber) {
		this.accessNumber = accessNumber;
	}
	public String getGatewayName() {
		return gatewayName;
	}
	public void setGatewayName(String gatewayName) {
		this.gatewayName = gatewayName;
	}
	
}
