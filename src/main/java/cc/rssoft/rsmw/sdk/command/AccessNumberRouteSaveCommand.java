package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.bean.AccessNumberRoute;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class AccessNumberRouteSaveCommand extends AbstractApiCommand {

	private String command = "accessNumberRouteSave";

	private AccessNumberRoute accessNumberRoute = null;

	public AccessNumberRouteSaveCommand(AccessNumberRoute accessNumberRoute) {
	    this.setAccessNumberRoute(accessNumberRoute);
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public AccessNumberRoute getAccessNumberRoute() {
		return accessNumberRoute;
	}

	public void setAccessNumberRoute(AccessNumberRoute accessNumberRoute) {
		this.accessNumberRoute = accessNumberRoute;
	}



}
