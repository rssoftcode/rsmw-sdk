package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class QueueGetAllCommand extends AbstractApiCommand {

	private String command = "queueGetAll";

	public QueueGetAllCommand() {
		
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

}
