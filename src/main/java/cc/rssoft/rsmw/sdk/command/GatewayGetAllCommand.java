package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class GatewayGetAllCommand extends AbstractApiCommand {

	private String command = "gatewayGetAll";

	public GatewayGetAllCommand() {
		
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

}
