package cc.rssoft.rsmw.sdk.command.internal;

public class SimpleResponse extends AbstractApiResponse{
	

	private Long id = null;

	private Object result = null;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

	
}
