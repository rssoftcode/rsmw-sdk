package cc.rssoft.rsmw.sdk.command.response;

import cc.rssoft.rsmw.sdk.command.bean.Queue;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;

public class QueueMemberPauseResponse extends AbstractApiResponse {

	private Queue queue;

	public Queue getQueue() {
		return queue;
	}

	public void setQueue(Queue queue) {
		this.queue = queue;
	}

	
	
}
