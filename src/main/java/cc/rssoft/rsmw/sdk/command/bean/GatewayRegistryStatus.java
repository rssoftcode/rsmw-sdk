package cc.rssoft.rsmw.sdk.command.bean;

public enum GatewayRegistryStatus {
	
	Registered("Registered","注册成功"),//绿色
	No_Authentication("No Authentication","鉴权失败"),//红色
	Request_Sent("Request Sent", "已发送注册请求"),//橙色
	Auth_Sent("Auth. Sent","已发送鉴权请求"),//橙色
	Unregistered("Unregistered","取消注册"),//灰色
	UNKNOW("UNKNOW","未知"),//灰色
;
		
	private String status;
	private String description;
	
	private GatewayRegistryStatus(String status, String description){
		this.setStatus(status);
		this.setDescription(description);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


}
