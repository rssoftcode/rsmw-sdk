package cc.rssoft.rsmw.sdk.command;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class GetBindInfoCommand extends AbstractApiCommand {

	private String command = "getBindInfo";
	
	private String agentAid = "";
	
	private String sipPhoneName = "";

	/**
	 * 两个参数，如果按座席查询绑定关系，则sipPhoneName传null，反之agentAid传null
	 * @param agentAid
	 * @param sipPhoneName
	 */
	public GetBindInfoCommand(String agentAid, String sipPhoneName) {
		this.agentAid = agentAid;
		this.sipPhoneName = sipPhoneName;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getAgentAid() {
		return agentAid;
	}

	public void setAgentAid(String agentAid) {
		this.agentAid = agentAid;
	}

	public String getSipPhoneName() {
		return sipPhoneName;
	}

	public void setSipPhoneName(String sipPhoneName) {
		this.sipPhoneName = sipPhoneName;
	}
	

}
