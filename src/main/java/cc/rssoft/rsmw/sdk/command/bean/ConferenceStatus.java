package cc.rssoft.rsmw.sdk.command.bean;

import java.util.LinkedHashMap;
import java.util.Map;

public class ConferenceStatus {

	//key = 会议室成员通话的channal
	Map<String, ConferenceMember> confbridgeListEventMap = new LinkedHashMap<>();

	//当前会议室
	private Conference conference = null;

	//是否繁忙
	private Boolean isBusy = null;

	//是否录音
	private Boolean isRecord = null;

	//是否锁定
	private Boolean locked = null;

	public Map<String, ConferenceMember> getConfbridgeListEventMap() {
		return confbridgeListEventMap;
	}

	public void setConfbridgeListEventMap(Map<String, ConferenceMember> confbridgeListEventMap) {
		this.confbridgeListEventMap = confbridgeListEventMap;
	}

	public Conference getConference() {
		return conference;
	}

	public void setConference(Conference conference) {
		this.conference = conference;
	}

	public Boolean getIsBusy() {
		return isBusy;
	}

	public void setIsBusy(Boolean isBusy) {
		this.isBusy = isBusy;
	}

	public Boolean getIsRecord() {
		return isRecord;
	}

	public void setIsRecord(Boolean isRecord) {
		this.isRecord = isRecord;
	}

	public Boolean getLocked() {
		return locked;
	}

	public void setLocked(Boolean locked) {
		this.locked = locked;
	}

}
