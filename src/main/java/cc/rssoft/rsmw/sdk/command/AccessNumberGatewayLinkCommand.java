package cc.rssoft.rsmw.sdk.command;

import java.util.List;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiCommand;

public class AccessNumberGatewayLinkCommand extends AbstractApiCommand {

	private String command = "accessNumberGatewayLink";

	private Long accessNumberId = null;
	private List<Long> gatewayIdList = null;

	public AccessNumberGatewayLinkCommand(Long accessNumberId, List<Long> gatewayIdList) {
		setAccessNumberId(accessNumberId);
		setGatewayIdList(gatewayIdList);
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public Long getAccessNumberId() {
		return accessNumberId;
	}

	public void setAccessNumberId(Long accessNumberId) {
		this.accessNumberId = accessNumberId;
	}

	public List<Long> getGatewayIdList() {
		return gatewayIdList;
	}

	public void setGatewayIdList(List<Long> gatewayIdList) {
		this.gatewayIdList = gatewayIdList;
	}

}
