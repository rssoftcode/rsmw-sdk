package cc.rssoft.rsmw.sdk.command.response;

import java.util.List;

import cc.rssoft.rsmw.sdk.command.bean.AccessNumber;
import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;

public class AccessNumberGetAllResponse extends AbstractApiResponse {

	private List<AccessNumber> result = null;

	public List<AccessNumber> getResult() {
		return result;
	}

	public void setResult(List<AccessNumber> result) {
		this.result = result;
	}

}
