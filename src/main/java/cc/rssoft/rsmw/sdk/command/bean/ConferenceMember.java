package cc.rssoft.rsmw.sdk.command.bean;

public class ConferenceMember {

	private String channel = null;
	private String conference = null;
	private String conferenceMemberAccessNumber = null;
	private String conferenceMemberGateway = null;
	private Boolean conferenceMemberLineStates = null;
	private String muted = null;
	private String phonenumber = null;

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getConference() {
		return conference;
	}

	public void setConference(String conference) {
		this.conference = conference;
	}

	public String getConferenceMemberAccessNumber() {
		return conferenceMemberAccessNumber;
	}

	public void setConferenceMemberAccessNumber(String conferenceMemberAccessNumber) {
		this.conferenceMemberAccessNumber = conferenceMemberAccessNumber;
	}

	public String getConferenceMemberGateway() {
		return conferenceMemberGateway;
	}

	public void setConferenceMemberGateway(String conferenceMemberGateway) {
		this.conferenceMemberGateway = conferenceMemberGateway;
	}

	public Boolean getConferenceMemberLineStates() {
		return conferenceMemberLineStates;
	}

	public void setConferenceMemberLineStates(Boolean conferenceMemberLineStates) {
		this.conferenceMemberLineStates = conferenceMemberLineStates;
	}

	public String getMuted() {
		return muted;
	}

	public void setMuted(String muted) {
		this.muted = muted;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

}
