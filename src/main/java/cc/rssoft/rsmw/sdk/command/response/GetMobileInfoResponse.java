package cc.rssoft.rsmw.sdk.command.response;

import cc.rssoft.rsmw.sdk.command.internal.AbstractApiResponse;

public class GetMobileInfoResponse extends AbstractApiResponse {

	private String areaCode = "";

	private String province = "";
	
	private String city = "";
	
	private String operator = "";

	/**
	 * 区号
	 * @return areaCode
	 */
	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	/**
	 * 省
	 * @return province
	 */
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	/**
	 * 市
	 * @return city
	 */
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * 运营商
	 * @return operator
	 */
	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

}
