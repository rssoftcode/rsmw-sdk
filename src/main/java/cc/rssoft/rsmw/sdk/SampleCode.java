package cc.rssoft.rsmw.sdk;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cc.rssoft.rsmw.sdk.command.bean.AccessNumber;
import cc.rssoft.rsmw.sdk.command.bean.AccessNumberRoute;
import cc.rssoft.rsmw.sdk.command.bean.Agent;
import cc.rssoft.rsmw.sdk.command.bean.Blacklist;
import cc.rssoft.rsmw.sdk.command.bean.Gateway;
import cc.rssoft.rsmw.sdk.command.bean.Queue;
import cc.rssoft.rsmw.sdk.command.bean.SipPhone;
import cc.rssoft.rsmw.sdk.command.internal.SimpleResponse;
import cc.rssoft.rsmw.sdk.command.response.AccessNumberGetAllResponse;
import cc.rssoft.rsmw.sdk.command.response.AccessNumberRouteGetAllResponse;
import cc.rssoft.rsmw.sdk.command.response.AgentGetAllResponse;
import cc.rssoft.rsmw.sdk.command.response.AgentSipPhoneBindInfoResponse;
import cc.rssoft.rsmw.sdk.command.response.BlacklistGetAllResponse;
import cc.rssoft.rsmw.sdk.command.response.GatewayGetAllResponse;
import cc.rssoft.rsmw.sdk.command.response.GetCdrByCallUuidResponse;
import cc.rssoft.rsmw.sdk.command.response.GetCdrByUniqueidResponse;
import cc.rssoft.rsmw.sdk.command.response.GetChannelCountResponse;
import cc.rssoft.rsmw.sdk.command.response.GetChannelResponse;
import cc.rssoft.rsmw.sdk.command.response.GetConferenceStatusResponse;
import cc.rssoft.rsmw.sdk.command.response.GetGatewayStatusResponse;
import cc.rssoft.rsmw.sdk.command.response.GetMobileInfoResponse;
import cc.rssoft.rsmw.sdk.command.response.GetQueueStatusResponse;
import cc.rssoft.rsmw.sdk.command.response.GetSipPhoneStatusResponse;
import cc.rssoft.rsmw.sdk.command.response.IsExtenResponse;
import cc.rssoft.rsmw.sdk.command.response.IsPhoneNumberBlockInResponse;
import cc.rssoft.rsmw.sdk.command.response.IsPhoneNumberBlockOutResponse;
import cc.rssoft.rsmw.sdk.command.response.OriginateResponse;
import cc.rssoft.rsmw.sdk.command.response.QueueGetAllResponse;
import cc.rssoft.rsmw.sdk.command.response.QueueMemberPauseResponse;
import cc.rssoft.rsmw.sdk.command.response.SipPhoneGetAllResponse;
import cc.rssoft.rsmw.sdk.command.response.VoiceVerificationCodeResponse;

/**
 * @version 2.0
 */
public class SampleCode {

    /**
     * 注意！！！！！！！！
     * 
     * rsmwUrl 格式请参考 RSMW 2 API 文档
     */
    private static String rsmwUrl = "http://192.168.8.227:18080/rsmw/api/2.0";

    /**
     * 注意！！！！！！！！
     * 
     * apiSecret在rsmw系统的API界面中配置。 即：启用apiSecret后的apiSecret
     */
    private static String apiSecret = "pA55w0rd";

    private static int connectTimeout = 2000;
    private static int readTimeout = 2000;

    // 初始化构造
    private static RsmwServer rsmwServer = new RsmwServer(rsmwUrl, apiSecret, connectTimeout, readTimeout);

    public static void main(String[] args) {

        
    }

    public static void main1(String[] args) {

        /**
         * 5.1 分机的操作接口
         * 
         * 【新增分机】
         */
        SipPhone sipPhoneSave = new SipPhone();
        // 基础
        sipPhoneSave.setName("8004");// 分机号(必填)
        sipPhoneSave.setSecret("aaa111");// 密码(必填)
        sipPhoneSave.setTimeout(60);// 无应答超时（秒）(必填)
        // 直线（DID）
        // sipPhone.setCallerIdNumber("1564353");//主叫号(选填) 填写接入号
        // sipPhone.setDstGateway("wangguanName");//网关(选填) 填写网关名称
        // 呼叫转移
        // sipPhone.setCallForwardType("none");//类型(选填)
        // 无[none]无条件转移[forwardAll]遇忙转移[forwardOnBusy]无应答转移[forwardOnNoanswer]
        // sipPhone.setCallForwardNumber("8007819");//转移至(选填)
        // sipPhone.setCallForwardAccessNumber("1564353");//主叫号(选填) 填写接入号
        // sipPhone.setCallForwardGateway("wangguanName");//网关(选填) 填写网关名称
        // 编码
        sipPhoneSave.setCodec1("all");// 语音编码1(必填)
                                      // 无[none]自动[all]G.711a[alaw]G.711u[ulaw]G.729[g729]G.723[g723]G.726[g726]G.722[g722]GSM[gsm]iLBC[ilbc]
        sipPhoneSave.setCodec2("none");// 语音编码2(必填)
                                       // 无[none]自动[all]G.711a[alaw]G.711u[ulaw]G.729[g729]G.723[g723]G.726[g726]G.722[g722]GSM[gsm]iLBC[ilbc]
        sipPhoneSave.setCodec3("none");// 语音编码3(必填)
                                       // 无[none]自动[all]G.711a[alaw]G.711u[ulaw]G.729[g729]G.723[g723]G.726[g726]G.722[g722]GSM[gsm]iLBC[ilbc]
        sipPhoneSave.setCodec4("none");// 语音编码4(必填)
                                       // 无[none]自动[all]G.711a[alaw]G.711u[ulaw]G.729[g729]G.723[g723]G.726[g726]G.722[g722]GSM[gsm]iLBC[ilbc]
        // 权限
        sipPhoneSave.setCanNotCallWithoutBind(false);// 绑定座席才可呼叫(必填)
        sipPhoneSave.setCanLdd(true);// 可以直播本地电话(必填)
        sipPhoneSave.setCanDdd(true);// 可以直播国内长途(必填)
        sipPhoneSave.setCanIdd(false);// 可以直播国际长途(必填)
        // 号码设置
        sipPhoneSave.setHideCallerId(false);// 隐藏号码(必填)
        
        SimpleResponse sipPhoneSaveResponse = rsmwServer.sipPhoneSave(sipPhoneSave);
        System.out.println(sipPhoneSaveResponse.toJsonString());

        /**
         * 【删除分机】
         */
        List<Long> sipPhoneIdList = new ArrayList<>();
        sipPhoneIdList.add(1L);
        sipPhoneIdList.add(2L);
        SimpleResponse sipPhoneDeleteResopnse = rsmwServer.sipPhoneDelete(sipPhoneIdList);
        System.out.println(sipPhoneDeleteResopnse.toJsonString());

        /**
         * 【修改分机】
         */
        SipPhone sipPhoneUpdate = new SipPhone();
        // 修改的ID
        sipPhoneUpdate.setId(1L);// 修改的ID(必填)
        // 基础
        sipPhoneUpdate.setName("8004");// 分机号(必填)
        sipPhoneUpdate.setSecret("aaa111");// 密码(必填)
        sipPhoneUpdate.setTimeout(60);// 无应答超时（秒）(必填)
        // 直线（DID）
        // sipPhone.setCallerIdNumber("1564353");//主叫号(选填) 填写接入号
        // sipPhone.setDstGateway("wangguanName");//网关(选填) 填写网关名称
        // 呼叫转移
        // sipPhone.setCallForwardType("none");//类型(选填)
        // 无[none]无条件转移[forwardAll]遇忙转移[forwardOnBusy]无应答转移[forwardOnNoanswer]
        // sipPhone.setCallForwardNumber("8007819");//转移至(选填)
        // sipPhone.setCallForwardAccessNumber("1564353");//主叫号(选填) 填写接入号
        // sipPhone.setCallForwardGateway("wangguanName");//网关(选填) 填写网关名称
        // 编码
        sipPhoneUpdate.setCodec1("all");// 语音编码1(必填)
                                        // 无[none]自动[all]G.711a[alaw]G.711u[ulaw]G.729[g729]G.723[g723]G.726[g726]G.722[g722]GSM[gsm]iLBC[ilbc]
        sipPhoneUpdate.setCodec2("none");// 语音编码2(必填)
                                         // 无[none]自动[all]G.711a[alaw]G.711u[ulaw]G.729[g729]G.723[g723]G.726[g726]G.722[g722]GSM[gsm]iLBC[ilbc]
        sipPhoneUpdate.setCodec3("none");// 语音编码3(必填)
                                         // 无[none]自动[all]G.711a[alaw]G.711u[ulaw]G.729[g729]G.723[g723]G.726[g726]G.722[g722]GSM[gsm]iLBC[ilbc]
        sipPhoneUpdate.setCodec4("none");// 语音编码4(必填)
                                         // 无[none]自动[all]G.711a[alaw]G.711u[ulaw]G.729[g729]G.723[g723]G.726[g726]G.722[g722]GSM[gsm]iLBC[ilbc]
        // 权限
        sipPhoneUpdate.setCanNotCallWithoutBind(false);// 绑定座席才可呼叫(必填)
        sipPhoneUpdate.setCanLdd(true);// 可以直播本地电话(必填)
        sipPhoneUpdate.setCanDdd(true);// 可以直播国内长途(必填)
        sipPhoneUpdate.setCanIdd(false);// 可以直播国际长途(必填)
        // 号码设置
        sipPhoneUpdate.setHideCallerId(false);// 隐藏号码(必填)

        SimpleResponse sipPhoneUpdateResponse = rsmwServer.sipPhoneUpdate(sipPhoneUpdate);
        System.out.println(sipPhoneUpdateResponse.toJsonString());

        /**
         * 【查询所有分机】
         */
        SipPhoneGetAllResponse sipPhoneGetAllResopnse = rsmwServer.sipPhoneGetAll();
        System.out.println(sipPhoneGetAllResopnse.toJsonString());

        /**
         * 【置忙分机】
         */
        QueueMemberPauseResponse qMemberPauseResponse = rsmwServer.queueMemberPause("SIP/8001", "去火星了。");//参数1：SIP/分机号 参数2：置忙理由
        System.out.println(qMemberPauseResponse.toJsonString());

        /**
         * 【置闲分机】
         */
        QueueMemberPauseResponse qMemberUnPauseResponse = rsmwServer.queueMemberUnPause("SIP/8001");//参数1：SIP/分机号
        System.out.println(qMemberUnPauseResponse.toJsonString());
        
        /**
         * 5.2 【发起呼叫】 呼叫方式一
         * 
         * @param absoluteTimeout
         *            通话超时时间（秒）。超时后自动挂断。0为永不超时
         * @param src
         *            第一路呼叫的号码
         * @param srcGateway
         *            第一路呼叫所需要走的gateway。若src为内部分机号，则此字段无需填写
         * @param srcAccessNumber
         *            第一路呼叫所需要用的主叫号。若src为内部分机号，则此字段无需填写
         * @param srcCallerIdName
         *            第一路呼叫所需要用的主叫（显示用）
         * @param srcCallerIdNumber
         *            第一路呼叫所需要用的主叫（显示用）
         * @param srcAnnounceMediaUrl
         *            第一路呼叫接通后所听到的提示音
         * @param dst
         *            第二路呼叫的号码
         * @param dstGateway
         *            第二路呼叫所需要走的gateway。若dst为内部分机号，则此字段无需填写
         * @param dstAccessNumber
         *            第二路呼叫所需要用的主叫号。若dst为内部分机号，则此字段无需填写
         * @param dstAnnounceMediaUrl
         *            第二路呼叫接通后所听到的提示音
         * @param userData
         *            自定义数据。会随CdrEvent返回
         */
        OriginateResponse originateResponse1 = rsmwServer.originate(0, "8001", "", "", "", "", "", "8002", "", "", "",
                "{}");
        System.out.println(originateResponse1.toJsonString());
        
        /**
         * 呼叫方式2
         * 适用于不显示主叫号和呼叫不带自定义数据。
         */
        OriginateResponse originateResponse2 = rsmwServer.originate(0, "8001","", "", "", "8002", "", "", "");
        System.out.println(originateResponse2.toJsonString());
        
        /**
         * 呼叫方式3
         * 适用于两个分机号互打
         */
        OriginateResponse originateResponse3 = rsmwServer.originate("8001", "8002");
        System.out.println(originateResponse3.toJsonString());
        
        /**
         * 5.3 【获取分机通话Channel和对Channel的操作】
         */
        GetChannelResponse getChannelResponse = rsmwServer.getChannel("8001");
        System.out.println(getChannelResponse.toJsonString());
        
        /**
         * 【桥接channel】
         */
        SimpleResponse bridgeResponse = rsmwServer.bridge("SIP/8001-XXXXXXXX", "SIP/8002-XXXXXXXX");
        System.out.println(bridgeResponse.toJsonString());

        /**
         * 【转接Channel】
         */
        SimpleResponse redirectResponse = rsmwServer.redirect("SIP/8001-XXXXXXXX", "_moh", "13391026171",
                "SIP/8002-XXXXXXXX", "core_route", "13391026172");
        System.out.println(redirectResponse.toJsonString());

        /**
         * 【挂断Channel】
         */
        SimpleResponse hangupResponse = rsmwServer.hangup("SIP/8001-XXXXXXXX");
        System.out.println(hangupResponse.toJsonString());
        
        /**
         * 5.4 对接座席的操作接口
         * 
         * 新增/删除/获取所有座席/同步座席
         * 
         * 注意：不支持修改座席，如需修改座席请删除后新建，并重新绑定相应的分机。
         * 
         * 【新增座席】
         */
        Agent agent = new Agent();
        agent.setPwd("password123");// 座席密码
        agent.setAgentName("张三");// 座席名称
        agent.setAid("21232F297A57A5A743894A0E4A801FC3");// 座席系统唯一标识
        agent.setJobNumber(9527L);// 座席工号
        // 请求
        SimpleResponse agentSaveResponse = rsmwServer.agentSave(agent);
        System.out.println(agentSaveResponse.toJsonString());

        /**
         * 【删除座席】
         */
        List<String> agentAidList = new ArrayList<>();// 座席系统唯一标识List集合
        agentAidList.add("21232F297A57A5A743894A0E4A801FC3");// 座席系统唯一标识
        agentAidList.add("0192023A7BBD73250516F069DF18B500");// 座席系统唯一标识
        SimpleResponse agentDeleteResponse = rsmwServer.agentDelete(agentAidList);
        System.out.println(agentDeleteResponse.toJsonString());

        /**
         * 【获取所有座席】
         */
        AgentGetAllResponse agentListAllResponse = rsmwServer.agentGetAll();
        System.out.println(agentListAllResponse.toJsonString());

        /**
         * 【同步座席】
         * 
         * 注意：同步会删除座席之前所有的数据，以同步数据为准。
         */
        List<Agent> agentList = new ArrayList<>();
        Agent a1 = new Agent();
        a1.setPwd("password123");// 座席密码
        a1.setAgentName("张三");// 座席名称
        a1.setAid("21232F297A57A5A743894A0E4A801FC3");// 座席系统唯一标识
        a1.setJobNumber(9527L);// 座席工号

        Agent a2 = new Agent();
        a2.setPwd("password456");// 座席密码
        a2.setAgentName("李四");// 座席名称
        a2.setAid("0192023A7BBD73250516F069DF18B500");// 座席系统唯一标识
        a2.setJobNumber(9528L);// 座席工号

        agentList.add(a1);
        agentList.add(a2);
        SimpleResponse agentSyncResponse = rsmwServer.agentSync(agentList);
        System.out.println(agentSyncResponse.toJsonString());

        /**
         * 5.5 座席与分机的操作接口
         * 
         * 【绑定】
         */
        SimpleResponse agentSipPhoneBindResponse = rsmwServer.bind("21232F297A57A5A743894A0E4A801FC3", "8001");
        System.out.println(agentSipPhoneBindResponse.toJsonString());

        /**
         * 【解绑方式一】 根据座席Aid解绑分机
         */
        SimpleResponse agentSipPhoneUnBindResponse1 = rsmwServer.unBind("21232F297A57A5A743894A0E4A801FC3", null);
        System.out.println(agentSipPhoneUnBindResponse1.toJsonString());

        /**
         * 【解绑方式二】 根据分机号解绑座席
         */
        SimpleResponse agentSipPhoneUnBindResponse2 = rsmwServer.unBind(null, "8001");
        System.out.println(agentSipPhoneUnBindResponse2.toJsonString());

        /**
         * 【绑定关系查询】 查询座席绑定的分机
         */
        AgentSipPhoneBindInfoResponse agentSipPhoneBindInfoResponse1 = rsmwServer.getBindInfo("9999", null);
        System.out.println(agentSipPhoneBindInfoResponse1.toJsonString());

        /**
         * 【绑定关系查询】查询分机绑定的座席
         */
        AgentSipPhoneBindInfoResponse agentSipPhoneBindInfoResponse2 = rsmwServer.getBindInfo(null, "8001");
        System.out.println(agentSipPhoneBindInfoResponse2.toJsonString());

        /**
         * 5.6 【实时并发数查询】
         */
        GetChannelCountResponse getChannelCountResponse = rsmwServer.getChannelCount();
        System.out.println(getChannelCountResponse.toJsonString());

        /**
         * 5.7 【呼叫保持】
         */
        SimpleResponse holdResponse = rsmwServer.hold("8001");
        System.out.println(holdResponse.toJsonString());
        /**
         * 取消呼叫保持
         */
        SimpleResponse unholdResponse = rsmwServer.unhold("8001");
        System.out.println(unholdResponse.toJsonString());

        /**
         * 5.8 【获取所有SipPhone状态】
         */
        GetSipPhoneStatusResponse getSipPhoneStatusResponse = rsmwServer.getSipPhoneStatus();
        System.out.println(getSipPhoneStatusResponse.toJsonString());

        /**
         * 5.9 【获取所有Gateway状态】
         */
        GetGatewayStatusResponse getGatewayStatusResponse = rsmwServer.getGatewayStatus();
        System.out.println(getGatewayStatusResponse.toJsonString());

        /**
         * 5.10 获取所有技能组状态
         */
        GetQueueStatusResponse getQueueStatusResponse = rsmwServer.getQueueStatus();
        System.out.println(getQueueStatusResponse.toJsonString());
        
        /**
         * 5.11 【查询归属地】 商业支持联系 13370021000 许玉亮
         */
        GetMobileInfoResponse getMobileInfoResponse = rsmwServer.getMobileInfo("13370021000");
        System.out.println(getMobileInfoResponse.toJsonString());

        /**
         * 5.12 【查询cdr】
         */
        GetCdrByCallUuidResponse response1 = rsmwServer.getCdrByCallUuid("f97ea90287244c87b2d6d4b60da61d16");
        System.out.println(response1.toJsonString());
        GetCdrByUniqueidResponse response2 = rsmwServer.getCdrByUniqueid("1493866989.59");
        System.out.println(response2.toJsonString());

        /**
         * 5.13【服务质量评价】
         */
        SimpleResponse sqeResponse = rsmwServer.sqe("8001", "http://ip:port/question.wav",
                "http://ip:port/thankyou.wav");
        System.out.println(sqeResponse.toJsonString());

        /**
         * 5.14 【判断某个号码是否内部exte】
         */
        IsExtenResponse isExtenResponse = rsmwServer.isExten("8001");
        System.out.println(isExtenResponse.toJsonString());

        /**
         * 5.15 监听
         */
        SimpleResponse chanSpyResponse = rsmwServer.chanSpy("监听者所使用的分机", "被监听的分机");
        System.out.println(chanSpyResponse.toJsonString());
        
        /**
         * 5.16 语音验证码
         * phoneNumber  收听语音验证码的手机号 String      是
         *  gateway 呼叫phoneNumber所使用的线路或网关设备    String      是
         *   accessNumber    呼叫phoneNumber所使用的主叫号码   String      是
         *   exten   系统内置语音验证码dialplan的exten，默认9999  String      是
         *   media1  语音1的路径。语音文件在【语音资源】菜单上传。注意，不需要文件扩展名  String      是
         *   code    验证码 String      是
         *   media2  语音2的路径。语音文件在【语音资源】菜单上传。注意，不需要文件扩展名  String      是
         *   repeat  重复次数    String      是
         *   absoluteTimeout 通话超时时间（秒）。超时后自动挂断
         */
        String phoneNumber = "13370021000";
        String gateway = "wangguan";
        String accessNumber = "1234561";
        String exten = "9999";
        String media1 = "/rsmw/resource/queueannounce/queueThankyou";//无需后缀名
        String code = "2xd5";
        String media2 = "/rsmw/resource/queueannounce/silence1";//无需后缀名
        Integer repeat = 3;
        Integer absoluteTimeout = 60;
        VoiceVerificationCodeResponse voiceVerificationCodeResponse = rsmwServer.voiceVerificationCode(phoneNumber, gateway, accessNumber, exten, media1, code, media2, repeat, absoluteTimeout);
        System.out.println(voiceVerificationCodeResponse.toJsonString());
        
        /**
         * 5.17 黑名单
         */
        /**
         *  无修改如需修改请删除重新添加
         *  
         * 添加黑名单
         */
        Blacklist b = new Blacklist();
        b.setPhonenumber("15755381763");
        b.setTypee("BLOCK_OUT");// BLOCK_OUT[禁止呼出] BLOCK_IN[禁止呼入]
        // BLOCK_BOTH[禁止呼入呼出]
        b.setDescription("禁止呼出");
        
        SimpleResponse blacklistSaveResponse = rsmwServer.blacklistSave(b);
        System.out.println(blacklistSaveResponse.toJsonString());

        /**
         * 获取所有的黑名单
         */
        BlacklistGetAllResponse blacklistGetAllResponse = rsmwServer.blacklistGetAll();
        System.out.println(blacklistGetAllResponse.toJsonString());

        /**
         * 黑名单呼入查询
         */
        IsPhoneNumberBlockInResponse IsPhoneNumberBlockInResponse = rsmwServer.isPhoneNumberBlockIn("15755381763");
        System.out.println(IsPhoneNumberBlockInResponse.toJsonString());

        /**
         * 黑名单呼出查询
         */
        IsPhoneNumberBlockOutResponse isPhoneNumberBlockOutResponse = rsmwServer.isPhoneNumberBlockOut("15755381761");
        System.out.println(isPhoneNumberBlockOutResponse.toJsonString());

        /**
         * 删除黑名单
         */
        List<Long> idList = new ArrayList<>();
        idList.add(1L);
        idList.add(2L);
        SimpleResponse blacklistDeleteResponse = rsmwServer.blacklistDelete(idList);
        System.out.println(blacklistDeleteResponse.toJsonString());
        
        /**
         * 5.18 电话会议控制
         * 
         * 查询会议的状态
         */
        String exten1 = "10009001";
        GetConferenceStatusResponse response = rsmwServer.getConferenceStatus(exten1);
        System.out.println(response.toJsonString());

        /**
         * 邀请
         */
        String extenInvite = "10009001";
        List<String> phoneNumberListInvite = new ArrayList<>();
        phoneNumberListInvite.add("10008001");
        phoneNumberListInvite.add("10008004");
        String gatewayInvite = "1111";
        String accessNumberInvite = "2222";
        SimpleResponse response11 = rsmwServer.conferenceInvite(extenInvite, gatewayInvite, accessNumberInvite,
                phoneNumberListInvite);
        System.out.println(response11.toJsonString());

        /**
         * 重新邀请
         */
        String extenReInvite = "10009001";
        String phoneNumberReInvite = "10008001";
        SimpleResponse responseReInvite = rsmwServer.conferenceReInvite(extenReInvite, phoneNumberReInvite);
        System.out.println(responseReInvite.toJsonString());

        /**
         * 踢出
         */
        String extenKick = "10009001";
        String channelKick = "SIP/10008001-00000008";
        SimpleResponse responseKick = rsmwServer.conferenceKick(extenKick, channelKick);
        System.out.println(responseKick.toJsonString());

        /**
         * 静音
         */
        String extenMute = "10009001";
        String channelMute = "SIP/10008001-0000000c";
        SimpleResponse responseMute = rsmwServer.conferenceMute(extenMute, channelMute);
        System.out.println(responseMute.toJsonString());

        /**
         * 解除静音
         */
        String extenUnMute = "10009001";
        String channelUnMute = "SIP/10008001-0000000c";
        SimpleResponse responseUnMute = rsmwServer.conferenceUnMute(extenUnMute, channelUnMute);
        System.out.println(responseUnMute.toJsonString());

        /**
         * 会议室锁定
         */
        String extenLock = "10009001";
        SimpleResponse responseLock = rsmwServer.conferenceLock(extenLock);
        System.out.println(responseLock.toJsonString());

        /**
         * 会议室解锁
         */
        String extenUnlock = "10009001";
        SimpleResponse responseUnlock = rsmwServer.conferenceUnlock(extenUnlock);
        System.out.println(responseUnlock.toJsonString());
        
        
        /**
         * 5.19 【接入号】增/删/改/查
         * 接入号的增删改查
         */
        AccessNumber accessNumber1 = new AccessNumber();
        accessNumber1.setNumber("1111");
        SimpleResponse accessNumberSaveResopnse = rsmwServer.accessNumberSave(accessNumber1);
        System.out.println(accessNumberSaveResopnse.toJsonString());

        accessNumber1.setId(accessNumberSaveResopnse.getId());
        accessNumber1.setNumber("22222");
        SimpleResponse accessNumberUpdateResopnse = rsmwServer.accessNumberUpdate(accessNumber1);
        System.out.println(accessNumberUpdateResopnse.toJsonString());

        AccessNumberGetAllResponse accessNumberGetAllResopnse = rsmwServer.accessNumberGetAll();
        System.out.println(accessNumberGetAllResopnse.toJsonString());

        List<Long> accessNumberIdList = new ArrayList<>();
        accessNumberIdList.add(accessNumber1.getId());
        SimpleResponse accessNumberDeleteResopnse = rsmwServer.accessNumberDelete(accessNumberIdList);
        System.out.println(accessNumberDeleteResopnse.toJsonString());

        /**
         * 5.20 网关的增删改查
         */
        Gateway g = new Gateway();
        g.setName("gw0");
        g.setMaxChannel(10);
        g.setMode("REG_NO");
        SimpleResponse gatewaySaveResponse = rsmwServer.gatewaySave(g);
        System.out.println(gatewaySaveResponse.toJsonString());

        g.setId(gatewaySaveResponse.getId());
        g.setName("gw00");
        SimpleResponse gatewayUpdateResopnse = rsmwServer.gatewayUpdate(g);
        System.out.println(gatewayUpdateResopnse.toJsonString());

        GatewayGetAllResponse gatewayGetAllResopnse = rsmwServer.gatewayGetAll();
        System.out.println(gatewayGetAllResopnse.toJsonString());

        List<Long> gatewayIdList = new ArrayList<>();
        gatewayIdList.add(g.getId());
        SimpleResponse gatewayDeleteResopnse = rsmwServer.gatewayDelete(gatewayIdList);
        System.out.println(gatewayDeleteResopnse.toJsonString());

        /**
         * 5.21 网关和接入号的关系的绑定
         */
        Long accessNumberId = 10L;
        List<Long> linkGatewayIdList = new ArrayList<>();
        linkGatewayIdList.add(5L);
        SimpleResponse accessNumberGatewayLinkResponse = rsmwServer.accessNumberGatewayLink(accessNumberId,
                linkGatewayIdList);
        System.out.println(accessNumberGatewayLinkResponse.toJsonString());

        /**
         * 5.22 接入号路由的增删改查
         */
        AccessNumberRoute accessNumberRoute = new AccessNumberRoute();
        accessNumberRoute.setName("1111");
        accessNumberRoute.setSeq(1L);
        accessNumberRoute.setStartDate(new Date());
        accessNumberRoute.setEndDate(new Date());
        accessNumberRoute.setAccessNumber("6666");
        accessNumberRoute.setDestType("SIPPHONE");
        accessNumberRoute.setDest("8001");
        SimpleResponse accessNumberRouteSaveResponse = rsmwServer.accessNumberRouteSave(accessNumberRoute);
        System.out.println(accessNumberRouteSaveResponse.toJsonString());

        accessNumberRoute.setId(accessNumberRouteSaveResponse.getId());
        accessNumberRoute.setName("2222");
        SimpleResponse accessNumberRouteUpdateResopnse = rsmwServer.accessNumberRouteUpdate(accessNumberRoute);
        System.out.println(accessNumberRouteUpdateResopnse.toJsonString());

        AccessNumberRouteGetAllResponse accessNumberRouteGetAllResopnse = rsmwServer.accessNumberRouteGetAll();
        System.out.println(accessNumberRouteGetAllResopnse.toJsonString());

        List<Long> accessNumberRouteIdList = new ArrayList<>();
        accessNumberRouteIdList.add(accessNumberRoute.getId());
        SimpleResponse accessNumberRouteDeleteResopnse = rsmwServer.accessNumberRouteDelete(accessNumberRouteIdList);
        System.out.println(accessNumberRouteDeleteResopnse.toJsonString());

        /**
         * 5.23 技能组的增删改查
         */
        Queue queue = new Queue();
        queue.setName("1111");

        SimpleResponse queueSaveResponse = rsmwServer.queueSave(queue);
        System.out.println(queueSaveResponse.toJsonString());

        queue.setId(queueSaveResponse.getId());
        queue.setName("2222");
        SimpleResponse queueUpdateResopnse = rsmwServer.queueUpdate(queue);
        System.out.println(queueUpdateResopnse.toJsonString());

        QueueGetAllResponse queueGetAllResopnse = rsmwServer.queueGetAll();
        System.out.println(queueGetAllResopnse.toJsonString());

        List<Long> queueIdList = new ArrayList<>();
        queueIdList.add(queue.getId());
        SimpleResponse queueDeleteResopnse = rsmwServer.queueDelete(queueIdList);
        System.out.println(queueDeleteResopnse.toJsonString());

        /**
         * 5.24 【技能组成员-手机】增/删
         */
        SimpleResponse queueMemberSaveOutboundNumberResponse = rsmwServer.queueMemberSaveOutboundNumber(5L,
                "13391026171", "gw1", 5);
        System.out.println(queueMemberSaveOutboundNumberResponse.toJsonString());

        SimpleResponse queueMemberDeleteOutboundNumberResponse = rsmwServer.queueMemberDeleteOutboundNumber(5L,
                queueMemberSaveOutboundNumberResponse.getId());
        System.out.println(queueMemberDeleteOutboundNumberResponse.toJsonString());

        /**
         * 5.25 【技能组成员-SIP分机和座席】关联¶
         */
        
        List<Long> queueMemberSipPhoneIdList = new ArrayList<>();
        List<String> queueMemberAgentAidList = new ArrayList<>();
        SimpleResponse queueMemberSetSipPhoneOrAgentResponse = rsmwServer.queueMemberSetSipPhoneOrAgent(5L,
                queueMemberSipPhoneIdList, queueMemberAgentAidList);
        System.out.println(queueMemberSetSipPhoneOrAgentResponse.toJsonString());

    }

}
